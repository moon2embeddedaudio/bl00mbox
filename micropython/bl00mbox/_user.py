# SPDX-License-Identifier: CC0-1.0

import sys_bl00mbox

# note: consider the 'sys_bl00mbox' api super unstable for now pls :3

import math
import uctypes
import bl00mbox
from bl00mbox import _helpers as helpers
from bl00mbox._patches import _Patch as Patch


class Bl00mboxError(Exception):
    pass


def _make_signal(plugin, signal_num=0, core=None):
    if core is None:
        core = sys_bl00mbox.SignalCore(plugin._core, signal_num)
    is_input = bool(core.hints & sys_bl00mbox.SIGNAL_HINT_INPUT)
    is_output = bool(core.hints & sys_bl00mbox.SIGNAL_HINT_OUTPUT)
    if is_output == is_input:
        raise Bl00mboxError("signal must be either input or output")
    if is_input:
        signal = SignalInput(plugin, core)
    else:
        signal = SignalOutput(plugin, core)
    return signal


class ChannelMixer:
    def __init__(self, core):
        self._core = core

    def __repr__(self):
        ret = f"[channel mixer] ({len(self.connections)} connections)"
        for con in self.connections:
            ret += f"\n  {con.name} in {con._plugin._repr_no_signals()}"
        return ret

    @property
    def connections(self):
        cons = []
        signal_cores = self._core.get_connected_mx()
        for core in signal_cores:
            plugin = bl00mbox._plugins._get_plugin(core.plugin_core)
            cons.append(_make_signal(plugin, core=core))
        return cons


class ValueSwitch:
    def __init__(self, plugin, core):
        self._plugin = plugin
        self._core = core

    def __setattr__(self, key, value):
        if getattr(self, "_locked", False):
            if not value:
                return
            val = getattr(self, key, None)
            if val is None:
                return
            self._core.value = val
        else:
            super().__setattr__(key, value)

    def get_value_name(self, val):
        d = dict((k, v) for k, v in self.__dict__.items() if not k.startswith("_"))
        for k, v in d.items():
            if v == val:
                return k


class Signal:
    def __init__(self, plugin, core):
        self._core = core
        self._plugin = plugin
        self._mpx = core.mpx
        self._unit = core.unit
        self._description = core.description
        constants = {}

        another_round = True
        while another_round:
            another_round = False
            for k, char in enumerate(self._unit):
                if char == "{":
                    for j, stopchar in enumerate(self._unit[k:]):
                        if stopchar == "}":
                            const = self._unit[k + 1 : k + j].split(":")
                            constants[const[0]] = int(const[1])
                            self._unit = self._unit[:k] + self._unit[k + j + 1 :]
                            break
                    another_round = True
                    break

        if constants:
            self._unit = self._unit.strip()
            self.switch = ValueSwitch(plugin, self._core)
            for key in constants:
                setattr(self.switch, key, constants[key])
            self.switch._locked = True

        hint_list = []
        if core.hints & sys_bl00mbox.SIGNAL_HINT_INPUT:
            hint_list.append("input")
        if core.hints & sys_bl00mbox.SIGNAL_HINT_OUTPUT:
            hint_list.append("output")
        if core.hints & sys_bl00mbox.SIGNAL_HINT_TRIGGER:
            hint_list.append("trigger")
        if core.hints & sys_bl00mbox.SIGNAL_HINT_SCT:
            hint_list.append("pitch")
        if core.hints & sys_bl00mbox.SIGNAL_HINT_GAIN:
            hint_list.append("gain")
        if core.hints & sys_bl00mbox.SIGNAL_HINT_DEPRECATED:
            hint_list.append("deprecated")
        self._hints = "/".join(hint_list)

    @property
    def connections(self):
        cons = []
        signal_cores = self._core.get_connected()
        for core in signal_cores:
            plugin = bl00mbox._plugins._get_plugin(core.plugin_core)
            cons.append(_make_signal(plugin, core=core))
        if self._core.connected_mx:
            cons.append(ChannelMixer(None))
        return cons

    def __repr__(self):
        ret = self._no_desc()
        if len(self._description):
            ret += "\n  " + "\n  ".join(self._description.split("\n"))
        return ret

    def _no_desc(self):
        self._plugin._check_existence()

        ret = self.name
        if self._mpx != -1:
            ret += "[" + str(self._mpx) + "]"

        if len(self.unit):
            ret += " [" + self.unit + "]"

        ret += " [" + self.hints + "]: "

        direction = " <?> "
        val = self.value
        if isinstance(self, SignalInput):
            direction = " <= "
            if len(self.connections):
                val = self.connections[0].value
        elif isinstance(self, SignalOutput):
            direction = " => "

        ret += str(val)

        if getattr(self, "switch", None) is not None:
            value_name = self.switch.get_value_name(val)
            if value_name is not None:
                ret += " (" + value_name.lower() + ")"

        if self._core.hints & sys_bl00mbox.SIGNAL_HINT_SCT:
            ret += " / " + str(self.tone) + " semitones / " + str(self.freq) + "Hz"

        if self._core.hints & sys_bl00mbox.SIGNAL_HINT_GAIN:
            if self.mult == 0:
                ret += " / (mute)"
            else:
                ret += " / " + str(self.dB) + "dB / x" + str(self.mult)

        conret = []
        for con in self.connections:
            if isinstance(con, Signal):
                conret += [f"{direction}{con.name} in {con._plugin._repr_no_signals()}"]
            if isinstance(con, ChannelMixer):
                conret += [f"{direction}[channel mixer]"]
        if len(conret) > 1:
            nl = "\n "
            ret += nl + nl.join(conret)
        elif conret:
            ret += conret[0]
        return ret

    @property
    def name(self):
        return self._core.name

    @property
    def description(self):
        return self._core.description

    @property
    def unit(self):
        return self._core.unit

    @property
    def hints(self):
        return self._hints

    @property
    def value(self):
        return self._core.value

    @property
    def tone(self):
        return (self.value - (32767 - 2400 * 6)) / 200

    @tone.setter
    def tone(self, val):
        if isinstance(self, SignalInput):
            if (type(val) == int) or (type(val) == float):
                self.value = (32767 - 2400 * 6) + 200 * val
            if type(val) == str:
                self.value = helpers.note_name_to_sct(val)
        else:
            raise AttributeError("can't set output signal")

    @property
    def freq(self):
        tone = (self.value - (32767 - 2400 * 6)) / 200
        return 440 * (2 ** (tone / 12))

    @freq.setter
    def freq(self, val):
        if isinstance(self, SignalInput):
            tone = 12 * math.log(val / 440, 2)
            self.value = (32767 - 2400 * 6) + 200 * tone
        else:
            raise AttributeError("can't set output signal")

    @property
    def dB(self):
        if self.value == 0:
            return -9999
        return 20 * math.log((abs(self.value) / 4096), 10)

    @dB.setter
    def dB(self, val):
        if isinstance(self, SignalInput):
            self.value = int(4096 * (10 ** (val / 20)))
        else:
            raise AttributeError("can't set output signal")

    @property
    def mult(self):
        return self.value / 4096

    @mult.setter
    def mult(self, val):
        if isinstance(self, SignalInput):
            self.value = int(4096 * val)
        else:
            raise AttributeError("can't set output signal")

    def start(self, velocity=32767):
        if self.value > 0:
            self.value = -abs(velocity)
        else:
            self.value = abs(velocity)

    def stop(self):
        self.value = 0


class SignalOutput(Signal):
    @Signal.value.setter
    def value(self, val):
        if val is None:
            self._core.disconnect()
        elif isinstance(val, SignalInput):
            val << self
        elif isinstance(val, ChannelMixer):
            self._core.connect_mx()
        # fails silently bc of backwards compatibility :/
        # we'll deprecate this setter entirely someday, use rshift instead

    def __rshift__(self, other):
        if not (
            isinstance(other, SignalInput)
            or isinstance(other, ChannelMixer)
            or other is None
        ):
            raise TypeError(f"can't connect SignalOutput to {type(other)}")
        self.value = other

    def __lshift__(self, other):
        raise TypeError("output signals can't receive data from other signals")


class SignalInput(Signal):
    @Signal.value.setter
    def value(self, val):
        if val is None:
            self._core.disconnect()
        elif isinstance(val, SignalOutput):
            self._core.connect(val._core)
        elif (type(val) == int) or (type(val) == float):
            self._core.value = int(val)
        # fails silently bc of backwards compatibility :/

    def __lshift__(self, other):
        if not (
            isinstance(other, SignalOutput)
            or type(other) == int
            or type(other) == float
            or other is None
        ):
            raise TypeError(f"can't connect SignalInput to {type(other)}")
        self.value = other

    def __rshift__(self, other):
        raise TypeError("input signals can't send data to other signals")


class SignalMpxList:
    def __init__(self):
        self._list = []
        self._setattr_allowed = False

    def __len__(self):
        return len(self._list)

    def __getitem__(self, index):
        return self._list[index]

    def __setitem__(self, index, value):
        try:
            current_value = self._list[index]
        except:
            raise AttributeError("index does not exist")
        if isinstance(current_value, Signal):
            current_value.value = value
        else:
            raise AttributeError("signal does not exist")

    def __iter__(self):
        self._iter_index = 0
        return self

    def __next__(self):
        self._iter_index += 1
        if self._iter_index >= len(self._list):
            raise StopIteration
        else:
            return self._list[self._iter_index]

    def add_new_signal(self, signal, index):
        self._setattr_allowed = True
        index = int(index)
        if len(self._list) <= index:
            self._list += [None] * (1 + index - len(self._list))
        self._list[index] = signal
        self._setattr_allowed = False

    def __setattr__(self, key, value):
        """
        current_value = getattr(self, key, None)
        if isinstance(current_value, Signal):
            current_value.value = value
            return
        """
        if key == "_setattr_allowed" or getattr(self, "_setattr_allowed", True):
            super().__setattr__(key, value)
        else:
            raise AttributeError("signal does not exist")


class SignalList:
    def __init__(self, plugin):
        self._list = []
        for signal_num in range(plugin._core.num_signals):
            signal = _make_signal(plugin, signal_num=signal_num)
            self._list.append(signal)
            name = signal.name.split(" ")[0]
            if signal._mpx == -1:
                setattr(self, name, signal)
            else:
                # <LEGACY SUPPORT>
                setattr(self, name + str(signal._mpx), signal)
                # </LEGACY SUPPORT>
                current_list = getattr(self, name, None)
                if not isinstance(current_list, SignalMpxList):
                    setattr(self, name, SignalMpxList())
                getattr(self, name, None).add_new_signal(signal, signal._mpx)
        self._setattr_allowed = False

    def __setattr__(self, key, value):
        current_value = getattr(self, key, None)
        if isinstance(current_value, Signal):
            current_value.value = value
            return
        elif getattr(self, "_setattr_allowed", True):
            super().__setattr__(key, value)
        else:
            raise AttributeError("signal does not exist")


_channel_init_callback = None
_rms_base = 3 - 10 * math.log(32767 * 32767, 10)


def set_channel_init_callback(callback):
    global _channel_init_callback
    _channel_init_callback = callback


class Channel:
    _core_keys = (
        "name",
        "num_plugins",
        "clear",
        "volume",
        "foreground",
        "background_mute_override",
        "callback",
        "compute_rms",
        "delete",
    )

    # we are passing through a lot of methods/properties to _core, so why not subclass?
    # well, the core needs to run a destructor in the engine when it is garbage collected,
    # and we're not so sure if it still does that when subclassed. we never actually tested
    # it, but we made bad experiences. also this allows us to hide the sys_ methods from users.
    #
    # maybe the proper solution would be to move this entire thing to the backend, but if we
    # ever wanna play around with fancy things like (de)serialization this might lead to a bigger
    # code size. it's fine for now.

    def __setattr__(self, key, value):
        if key in self._core_keys:
            setattr(self._core, key, value)
        else:
            super().__setattr__(key, value)

    def __getattr__(self, key):
        # __getattr__ is only fallback, but since we don't allow for keys in _core_keys
        # to be stored within Channel this should suffice
        if key in self._core_keys:
            return getattr(self._core, key)
        else:
            raise AttributeError(f"'{type(self)}' object has no attribute {key}")

    def __init__(self, name="repl"):
        # never ever hand out the deep core to somebody else to make sure channel gets gc'd right
        self._deep_core = sys_bl00mbox.ChannelCore(name)
        self._core = self._deep_core.get_weak()
        # also don't hand out self, use this one instead
        self._weak_channel = WeakChannel(self._core)
        if _channel_init_callback is not None:
            _channel_init_callback(self)
        self._channel_plugin = bl00mbox._plugins._get_plugin(self._core.channel_plugin)

    @property
    def signals(self):
        return self._channel_plugin._signals

    def __repr__(self):
        ret = f'[channel "{self.name}"]'
        if self.foreground:
            ret += " (foreground)"
        if self.background_mute_override:
            ret += " (background mute override)"
        ret += "\n  gain: " + str(self.gain_dB) + "dB"
        b = self.num_plugins
        ret += "\n  plugins: " + str(b)
        ret += "\n  " + "\n  ".join(repr(self.mixer).split("\n"))
        return ret

    @property
    def free(self):
        # legacy oof
        return self._core is None

    @free.setter
    def free(self, val):
        # bigger legacy oof
        if val:
            self.clear()
            self._core = None

    def _new_plugin(self, thing, *args, **kwargs):
        if isinstance(thing, bl00mbox._plugins._PluginDescriptor):
            return bl00mbox._plugins._make_plugin(
                self._weak_channel, thing.plugin_id, *args, **kwargs
            )
        else:
            raise TypeError("not a plugin")

    def new(self, thing, *args, **kwargs):
        self.free = False
        if type(thing) == type:
            if issubclass(thing, bl00mbox.patches._Patch):
                return thing(self._weak_channel, *args, **kwargs)
        elif isinstance(thing, bl00mbox._plugins._PluginDescriptor) or (
            type(thing) == int
        ):
            return self._new_plugin(thing, *args, **kwargs)
        else:
            raise TypeError("invalid plugin/patch")

    @property
    def plugins(self):
        for core in self._core.get_plugins():
            yield bl00mbox._plugins._get_plugin(core)

    @property
    def gain_dB(self):
        ret = self._core.volume
        if ret == 0:
            return -math.inf
        else:
            return 20 * math.log(ret / 32768, 10)

    @gain_dB.setter
    def gain_dB(self, value):
        if value == -math.inf:
            value = 0
        else:
            value = int(32768 * (10 ** (value / 20)))
            value = min(32767, max(0, value))
        self._core.volume = value

    @property
    def rms_dB(self):
        if self.compute_rms:
            ret = self._core.mean_square
            if ret == 0:
                return -math.inf
            else:
                # volume is applied together with sys_volume
                # after rms is calculated. we do want the output
                # before sys_volume but after regular volume,
                # so we're compensating here
                mult = abs(self._core.volume) / 32768
                if mult == 0:
                    return -math.inf
                # squaring because we're getting root later
                ret *= mult * mult
                return 10 * math.log(ret, 10) + _rms_base
        else:
            return None

    @property
    def mixer(self):
        return ChannelMixer(self._core)

    @mixer.setter
    def mixer(self, val):
        if isinstance(val, SignalOutput):
            val.value = ChannelMixer(self._core)
        elif val is not None:
            # val is None: backwards compatibility, not allowed to throw exception ;w;
            raise Bl00mboxError("can't connect this")


class WeakChannel(Channel):
    def __init__(self, core):
        self._core = core.get_weak()
        self._channel_plugin = bl00mbox._plugins._get_plugin(self._core.channel_plugin)
        self._weak_channel = self


class SysChannel(Channel):
    def __init__(self, core):
        self._deep_core = core
        self._core = core.get_weak()
        self._channel_plugin = bl00mbox._plugins._get_plugin(self._core.channel_plugin)
        self._weak_channel = WeakChannel(self._core)

    @property
    def sys_gain_dB(self):
        ret = self._core.sys_gain
        if ret == 0:
            return -math.inf
        else:
            try:
                return 20 * math.log(ret / 4096, 10)
            except:
                return -math.inf

    @sys_gain_dB.setter
    def sys_gain_dB(self, value):
        if value == -math.inf:
            value = 0
        else:
            value = int(4096 * (10 ** (value / 20)))
            value = min(32767, max(0, value))
        self._core.sys_gain = value

    @property
    def sys_rms_dB(self):
        if self.compute_rms:
            ret = self._core.mean_square
            if ret == 0:
                return -math.inf
            else:
                mult = self._core.volume / 32768
                mult *= self._core.sys_gain / 4096
                if mult == 0:
                    return -math.inf
                ret *= mult * mult
                return 10 * math.log(ret, 10) + _rms_base
        else:
            return None


class Sys:
    @staticmethod
    def clear():
        cores = sys_bl00mbox.collect_channels(False)
        for core in cores:
            core.delete()

    @staticmethod
    def foreground_clear():
        cores = sys_bl00mbox.collect_channels(True)
        for core in cores:
            core.foreground = False

    @staticmethod
    def collect_channels(active, weak_ref=False):
        cores = sys_bl00mbox.collect_channels(active)
        for core in cores:
            if weak_ref:
                core = core.get_weak()
            yield SysChannel(core)

    @staticmethod
    def collect_active_callbacks():
        # Channel callbacks have a strange side effect on garbage collection:
        #
        # If a Channel has become unreachable but has not been collected yet, this will still fetch the
        # ChannelCore object and the attached callback, meaning that from a gc perspective the channel
        # "flickers" into reachability on the stack while the callback is fetched. if the callback
        # contains the last reference to the channel (as it typically does), this flickering is extended
        # for the duration of the callback execution.
        #
        # If garbage collection is triggered always in that exact moment, the channel will never
        # be collected. In practice, this is very unlikely, so it should be fine.
        #
        # It is still good practice to call .clear() on the Channel before dropping the last
        # reference as this will also remove the callback and set defined endpoint to Channel
        # audio rendering.
        #
        # We are relying on implementation details of the garbage collector to ensure memory safety.
        # micropython uses primitive conservative tracing gc triggered by some or the other threshold.
        # It always sweeps everything it can before it returns control. See channel_core_obj_t for details.
        #
        # We never heard of this technique before, probably because it's risky. On the off chance we
        # invented something here we'll dub this one "gc necromancy". Think of the flickering objects
        # as "tiny little ghosts" that get summoned during the midnight hour and can only be gc'd at
        # daytime :D. Necromancy is allowed to be risky business ^w^.
        #
        # hooray programming!
        for core in sys_bl00mbox.collect_channels(True):
            if core.callback and core.sys_gain:
                yield core.callback
