#pragma once
#include "bl00mbox_os.h"

typedef struct _bl00mbox_ll_t {
    struct _bl00mbox_ll_t * next;
    void * content;
} bl00mbox_ll_t;

typedef struct {
    size_t len;
    void * elems[];
} bl00mbox_array_t;

typedef bool (* bl00mbox_set_key_t)(void *, void *);

// initialize all zeroed out except for content type
typedef struct {
    bl00mbox_ll_t * start;
    bl00mbox_set_key_t key;
    size_t len;
} bl00mbox_set_t;

typedef struct {
    bl00mbox_ll_t * next;
}bl00mbox_set_iter_t;

bool bl00mbox_set_contains(bl00mbox_set_t * set, void * content);
bool bl00mbox_set_add_skip_unique_check(bl00mbox_set_t * set, void * content);
bool bl00mbox_set_add(bl00mbox_set_t * set, void * content);
bool bl00mbox_set_remove(bl00mbox_set_t * set, void * content);

// removing from set during iteration is safe, adding is NOT
void bl00mbox_set_iter_start(bl00mbox_set_iter_t * iter, bl00mbox_set_t * set);
void * bl00mbox_set_iter_next(bl00mbox_set_iter_t * iter);

bl00mbox_array_t * bl00mbox_set_to_array(bl00mbox_set_t * set);
