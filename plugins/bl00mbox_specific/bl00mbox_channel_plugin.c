#include "bl00mbox_channel_plugin.h"
#include "bl00mbox_config.h"

radspa_descriptor_t bl00mbox_channel_plugin_desc = {
    .name = "channel",
    .id = BL00MBOX_CHANNEL_PLUGIN_ID,
    .description = "bl00mbox channel signals",
    .create_plugin_instance = bl00mbox_channel_plugin_create,
    .destroy_plugin_instance = radspa_standard_plugin_destroy
};

// TODO: at this point we CANNOT just forward the same buffer pointer to every client because the buffer
// pointer serves as a channel-specific source plugin identifier, so we need to memcpy the line in.
// this could be faster at some point by adding a special case but we don't wanna think about it now.

typedef struct {
    int16_t buffer[BL00MBOX_MAX_BUFFER_LEN];
    uint32_t buffer_render_pass_id;
    int16_t value;
    int16_t value_render_pass_id;
} line_in_singleton_t;

static line_in_singleton_t line_in;

static inline void update_line_in_buffer(uint16_t num_samples, uint32_t render_pass_id){
    if(line_in.buffer_render_pass_id == render_pass_id) return;
    for(uint16_t i = 0; i < num_samples; i++){
        int32_t val = bl00mbox_line_in_interlaced[2*i];
        val += bl00mbox_line_in_interlaced[2*i+1];
        val = radspa_clip(val>>1);
        line_in.buffer[i] = val;
    }
    line_in.value = line_in.buffer[0];
    line_in.value_render_pass_id = render_pass_id;
    line_in.buffer_render_pass_id = render_pass_id;
}

static inline void update_line_in_value(uint32_t render_pass_id){
    if(line_in.value_render_pass_id == render_pass_id) return;
    int32_t val = bl00mbox_line_in_interlaced[0];
    val += bl00mbox_line_in_interlaced[1];
    val = radspa_clip(val>>1);
    line_in.value = val;
    line_in.value_render_pass_id = render_pass_id;
}

void bl00mbox_channel_plugin_run(radspa_t * channel_plugin, uint16_t num_samples, uint32_t render_pass_id){
    if(!bl00mbox_line_in_interlaced) return;

    // handle line in signal, only render full if requested
    radspa_signal_t * input_sig = radspa_signal_get_by_index(channel_plugin, 0);
    if(input_sig->buffer){
        update_line_in_buffer(num_samples, render_pass_id);
        memcpy(input_sig->buffer, line_in.buffer, sizeof(int16_t) * num_samples);
    }
    // do NOT render output here, if somebody has requested channel this would be too early
    // we're accessing the source buffer directly if present to avoid needing memcpy
}

void bl00mbox_channel_plugin_update_values(radspa_t * channel_plugin, uint32_t render_pass_id){
    update_line_in_value(render_pass_id);
    radspa_signal_t * input_sig = radspa_signal_get_by_index(channel_plugin, 0);
    input_sig->value = line_in.value;
}

radspa_t * bl00mbox_channel_plugin_create(uint32_t init_var){
    radspa_t * channel_plugin = radspa_standard_plugin_create(&bl00mbox_channel_plugin_desc, 2, 0, 0);
    if(!channel_plugin) return NULL;
    channel_plugin->render = bl00mbox_channel_plugin_run;
    radspa_signal_set(channel_plugin, 0, "line_in", RADSPA_SIGNAL_HINT_OUTPUT, 0);
    radspa_signal_set(channel_plugin, 1, "line_out", RADSPA_SIGNAL_HINT_INPUT, 0);
    return channel_plugin;
}
