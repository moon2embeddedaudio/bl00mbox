.. bl00mbox documentation master file, created by
   sphinx-quickstart on Sun Jun 11 19:43:11 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

bl00mbox
========

bl00mbox is a modular synthesizer engine with micropython bindings designed for 32bit microcontrollers without FPU.

bl00mbox uses it/bun pronouns.

Features
--------

What you can do with bl00mbox right now:

- **Low level sound design:** Build instruments or sound processors from atomic audio building blocks such as oscillators,
  filters and sequencers

- **Real-time interactions:** bl00mbox processes all external inputs at least once per 64-sample buffer, so at a sample rate
  of 48kHz its reaction time is never below 1.4ms. Micropython processing latencies still apply on top.

- **Plug anything into anything:** Parameters are exposed in the same manner as audio signals, you can modulate reverb mix
  with a bass note if you want to, even circular dependency loops are okay!

- **Create multi-channel soundscapes:** Organize plugin groups into channels and control them seperately in a global mixer
  with channel-specific micropython callbacks

Hardware
--------

What you can run bl00mbox on right now:

- `flow3r <https://flow3r.garden>`_ originated as the badge of the 2023 Chaos Communication Camp. bl00mbox was specifically
  designed to provide this project with a responsive behavior to match the very fast tap input method, expressive sound bending
  options from positional touch and a management system that enables independent applications to create sounds together.

.. toctree::
   :maxdepth: 1
   :caption: User Guide

   guide/bleepy.rst
   guide/channels_plugins.rst
   guide/signals.rst
   guide/patches.rst
   guide/examples.rst
   guide/troubleshooting.rst

.. toctree::
   :maxdepth: 1
   :caption: API

   api/bl00mbox.rst
   api/plugins.rst
   api/patches.rst
   api/helpers.rst

Stability
---------

This project is generally in an early stage. We are trying to approach basic stability, but some deprecations still need to happen.
since bun is already running on at least one user-facing device we try to keep backwards compatibility okay, but sometimes things
are broken for the sake of progress.

.. toctree::
   :maxdepth: 1
   :caption: Integration

   integration/requirements.rst
   integration/backend.rst
   integration/frontend.rst

bl00mbox can theoretically be included as-is in other ESP32 micropython projects, but the system integration API is somewhat
rough and pitfall-y. Porting to other microcontrollers with existing micropython + FreeRTOS support is relatively easy aside
from performance details. We don't recommend porting it to systems without basic operating system primitives.

Plugin support
--------------

Currently, bl00mbox only supports plugins in the custom-tailored ``radspa`` format. This format is currently not stable or
properly documented but can be previewed `here <https://gitlab.com/moon2embeddedaudio/radspa>`_.

Porting other formats is desirable, but we wish to focus on systems without FPU for now, so many common formats would perform
poorly. Of course, modern microcontrollers can provide amazing FPU performance and we will definitely branch out into that
at some point, we just don't want bl00mbox to depend on it.

Source code
-----------

https://gitlab.com/moon2embeddedaudio/bl00mbox
