Bleepy
======

Here's a minimal bl00mbox synth snipped to get you started:

.. code-block:: python

    import bl00mbox

    # request a new channel named "bleepy"
    blm = bl00mbox.Channel("bleepy")

    # use channel to create instances of plugins:
    # - an oscillator to produce a continuous tone
    osc = blm.new(bl00mbox.plugins.osc)
    # - an envelope generator to change volume of osc 
    env = blm.new(bl00mbox.plugins.env_adsr)

    # plug the oscillator into the envelope generator
    osc.signals.output >> env.signals.input
    # ..and the envelope output into the channel output
    blm.signals.line_out << env.signals.output

    # use the trigger input signal of env to play:
    # - send a "start" event to play tone
    env.signals.trigger.start()
    # - send a "stop" event to stop playing
    env.signals.trigger.stop()

    # use the pitch input signal of osc to change pitch
    # one semitone down via the .tone attribute
    osc.signals.pitch.tone -= 1
    
    # if we reduce the sustain parameter of an ADSR envelope
    # to 0 it stops automatically
    env.signals.sustain.value = 0

    # no .stop() needed this time
    env.signals.trigger.start()

You can find a complete list of plugin types and their signals
in the ``bl00mbox.plugins`` section.
