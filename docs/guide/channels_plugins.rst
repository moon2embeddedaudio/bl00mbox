Channels & Plugins
==================

Overview
--------

bl00mbox uses plugins as atomic sound generators/processors. These plugins provide signals which can be used to stream
audio or control data from one plugin to another. Arbitrary internal signals may then be routed to the device sound
inputs/outputs.

Plugins are grouped into channels so that operating systems can quickly switch between presets and/or mix different
sound generators/processors together; flow3r for example has a system mixer that also allows users to suspend rendering
of a channel by muting it, or a guitar pedal could have different sounds stored in RAM as channels and quickly switch
between them.

Create channels and plugins
---------------------------

Channels can be created directly with their default constructor. For user interfaces (like flow3r's system mixer) it is
nice to give descriptive names to channels, and we should always to so with a positional argument:

.. code-block:: python

    import bl00mbox

    # create a new channel
    blm = bl00mbox.Channel("example")

Plugin objects are stored in the ``bl00mbox.plugins`` module. Thheycannot be created directly, they must live within a channel.
This is done with the  ``.new()`` method that takes a plugin object type and returns an instance of it:

.. code-block:: python

    # create an instance of a plugin
    osc = blm.new(bl00mbox.plugins.osc)

This ``osc`` plugin is now tied to the ``blm`` channel. This means that its signals can be connected to other plugins
within that channel, or the channel signals themselves. Plugins cannot connect to signals of plugins from other channels.

Some plugins require additional initialization variables, the mixer for example needs a number of input channels. These
are listed in the section of the respective plugin in the ``bl00mbox.plugins`` section.

.. code-block:: python

    mixer = blm.new(bl00mbox.plugins.mixer, 5)

Plugins and channels may both be explicitely deleted with their respective ``.delete()`` methods. Plugins and connection
buffers can be quite memory intense, so we recommend cleaning up resources you don't need anymore.

Control channel rendering
-------------------------

Volume normalization
^^^^^^^^^^^^^^^^^^^^

Channels feature a volume control, DC blocking and an optional RMS volume tracker. Many operating systems expect a certain
volume level from their channels, check their documentation (for flow3r it's -15dB).

.. code-block:: python

    # activate RMS meaasurement (eats CPU, should only be enabled selectively)
    blm.compute_rms = True

    # read current RMS volume
    print(blm.rms_dB)

    # set channel volume (-inf..0, default -12)
    blm.gain_dB = -18

    # read updated RMS volume (will take a few 100ms to be stable around new value)
    print(blm.rms_dB)

Operating systems have their own volume control for each channel that they can provide to the user via a dedicated interface,
so we should normally not use the ``.gain_dB`` attribute for controlling volume in the mix other than maybe for a transient
fade-in/out.

Auto-foregrounding
^^^^^^^^^^^^^^^^^^

bl00mbox does some basic automatic channel management under the hood. Most importantly, there is no more than one channel
which is foregrounded. By default, only the foreground channel is rendered, all others are waiting in memory until their
foreground time has come. The engine auto-foregrounds whichever channel was last interacted with; this prevents users from
forgetting foregrounding and waiting in silence.

We can't quite recall what exactly constitutes an interaction, but we think most non-destructive operations on a channel,
its plugins and their signals count. Creating a new channel foregrounds it as well. A future version will probably include
means to switch off auto-foregrounding. You can always manually control with the ``.foreground`` attribute.

Background Mute Override
^^^^^^^^^^^^^^^^^^^^^^^^

Backgrounded channels may render as well using the ``.background_mute_override`` attribute. There can be arbitrary many
channels that render in the background. Typically, background channels do not receive most user input, they are intended
to generate sound mostly automatically while the user actively plays the foreground channel. Let's see a little render
roundabout:

.. code-block:: python

    drums = bl00mbox.Channel("drums")
    # [skipped: build some auto-drum patch, beat is now playing]

    lead = bl00mbox.Channel("lead")
    # [skipped: build some tonal patch, user is playing it with hardware elements somehow]]
    # "drums" is now silent since "lead" took the foreground

    lead.foreground = False
    # "drums" is still silent, nothing is in foreground

    drums.background_mute_override = True
    # "drums" starts playing in background

    # stop interacting with "lead" here, channel ringing out...

    # "drums" is in foreground again, "lead" stops ringing out since it doesn't render in background
    drums.foreground = True
    drums.background_mute_override = False

Channel Callbacks
^^^^^^^^^^^^^^^^^

You may attach a callback to a channel via the ``.callback`` attribute. The surrounding operating system may request
all callback objects from channels that are currently rendered and then do whatever with them; the callback format
is not defined by bl00mbox. If the operating system supports it, this allows you to effectively attach bespoke micropython
scripted plugins to your channel.

A big advantage of this is that they run in the (typically different, lower priority) micropython task, meaning a buffer
would never block on them, instead they get updated whenever there's free CPU resources after all audio rendering is done.

See the `flow3r documentation on its implementation <https://docs.flow3r.garden/app/guide/bl00mbox.html#run-in-background>`_
for a typical use pattern.

Garbage collection oddities
---------------------------

By default, objects created by a ``Channel`` contain no refernce to that channel object. If this object becomes unreachable
(i.e., all references are dropped) the channel including all of its plugins are garbace collected. Remaining references
to plugins will raise ``bl00mbox.ReferenceError`` when attempting to interact.

Plugins and patches are garbage collected when all references are dropped and they are not reachable in the render tree: If
a continuous connection pointing from a plugin signal through other plugins to a channel root signal (typically ``.line_out``),
it will not be garbage collected.

In both cases, garbage collection is identical to calling the ``.delete()`` method.

.. code-block:: python

    # delete channel and all plugins to free resources
    blm.delete()

    # osc is now a stale reference and the following will fail with ReferenceError:
    print(osc.signals.output.value)
