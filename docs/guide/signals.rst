Signals & Connections
=====================

In the previous section we learned how to create plugins instances, now let's do something with them!

Signals carry information, such as audio data or modulation data for say a filter plugin's cutoff
frequency. We store these ``Signal`` objects as attributes in ``SignalList`` objects, which are really
nothing more than containers that allow for convenient named access. Some signals are in arrays,
such as the input signals for a mixer, in which case we store them as a tuple of ``Signal`` objects
in the same way, so that:

.. code-block:: python

    mixer = blm.new(bl00mbox.plugins.mixer)
    env_adsr = blm.new(bl00mbox.plugins.env_adsr)
    osc = blm.new(bl00mbox.signals.osc)
    
    # check out value of a normal signal
    osc.signals.output.value
    # check out value of a member of a signal array
    mixer.signals.input[0].value
    
How do you know what's what? Signal behavior is generally highly individual and it is advised
to read the documentation of the plugin in the ``bl00mbox.plugins`` section when working with it.

Assigning static values
-----------------------

bl00mbox signals internally are encoded as integer values from -32767..32767. To access this
raw data we have used the ``.value`` attribute before; this is always possible. In case
of input signals, you can also assign static values to this attribute. If that signal
is connected to another signal it is disconnected when a static value is assigned.

.. code-block:: python

    # read the current value
    print(osc.signals.morph.value)

    # assign a fixed value to a signal and disconnect all sources
    osc.signals.morph.value = -12000

Connecting signals
------------------

A connection can only be made between an input and an output type signal:

.. code-block:: python

    # connecting an input to an output...
    env_adsr.signals.input << osc.signals.output
    # ...or an output to an input!
    env_adsr.signals.env_output >> osc.signals.morph
    # the channel has signals too
    env_adsr.signals.output >> blm.signals.line_out
    # listen :D
    env_adsr.signals.trigger.start()

An output signal can fan out to arbitrary many inputs, but an input may only receive data from a single source,
attempting to assign a different source or static value to it will result in the exiting connection being severed.
A signal may also be disconnected by setting it to `None`, in which case an input signal will fall back to its last
static value.

You can find out whether a signal is input or output either by checking the ``bl00mbox.plugins`` section or by
inspecting it via with ``print(Signal)``. There are subtypes that we'll go into later.

.. _sigconn-disconnect:

Disconnecting signals
---------------------

There's 4 ways to undo a connection:

For the receiving signal ``signal_rx``:

- Disconnect via ``signal_rx << None``. This restores the last static value that has been set or its default.
- Disconnect by setting a static value via ``signal_rx.value = 1000``.
- Disconnect by retaining the last value streamed from the existing connection. Since this value can be inspected
  via ``signal_rx.value`` prior to disconnecting, we can use this silly thing: ``signal_rx.value = signal_rx.value``

For the transmitting signal ``signal_tx``:

.. warning::

    It's easy to have forgotten what's connected to what, you might disconnect much more than you think.
    This might not be easy to undo and in the current state can even mess with patches in unstable ways. The
    ``buffer`` plugin provides a band-aid for this by isolating outputs.

- Disconnect via ``signal_tx >> None`` disconnects all receiving signals. This may have unwanted side effects.
  Check before use what's connected by inspecting it via ``print(signal_tx)``.

Standard signal types
---------------------

The mapping between signal value and how it modulates whatever property it affects is basically arbitrary, and you'll
find a detailed list in the ``bl00mbox.plugins`` section. However, there are a few standardized types that come with
special members. For encoding details, check out the ``bl00mbox.helpers`` section. These just wrap around ``.value``
and follow the same read/write rules.

For pitch-type signals:

.. py:attribute:: tone
    :type: float

    Setter/getter for musical pitch as semitone distance from A440.

.. py:attribute:: freq
    :type: float

    Setter/getter for frequency in Hertz.

For gain-type signals:

.. py:attribute:: dB
    :type: float

    Setter/getter for volume in dB

.. py:attribute:: mult
    :type: float

    Setter/getter for volume in expressed as a multiplier.

It is generally a bad idea to use a setter/getter that is not made for that signal type, but there are legitimate use
cases so we allow everything, for example:

.. code-block:: python

    range_shifter = blm.new(bl00mbox.plugins.range_shifter)
    range_shifter.signals.output >> osc.signals.pitch
    range_shifter.signals.input << noise.signals.output

    # set range with .freq attribute even though signals are not pitch-type
    range_shifter.signals.output_range[0].freq = 10
    range_shifter.signals.output_range[1].freq = 20


Some signals have discrete named value markers stored in the ``.switch`` member. Switched signals typically interpret their value as
rounded to the nearest marker while semi-switched signals allow for values in-between. The markers are subclassing ``int`` and
return their value when accessed, but a special setter allows them to be applied to the signal directly by setting them to a truthy
value. This is kind of weird API but it works okay.

.. code-block:: python

    # selecting the square wave
    osc.signals.wave.switch.SQUARE = True
    # using the integer values to interpolate between neighbors
    mixed = (osc.signals.wave.switch.TRI + osc.signals.wave.switch.SINE)/2
    osc.signals.wave = mixed

*Note: In case of a binary switch you might be tempted to assume that setting one marker to `False` activates the other. 
This is however not the case, the signal does nothing instead.*

Events
------

Events are generally transported by trigger-type signals. For example, if you want a few ``sampler`` plugins to play a beat, you could
connect their ``.signals.playback_trigger`` inputs to the ``.signals.track[]`` outputs of a ``sequencer`` plugin.

There's generally 2.5 types of events: ``start``, ``stop``, and ``restart`` (which is just another ``start`` event while a thingy is
already in the "started" state). We can generate them with the following ``Signal`` methods:

.. py:method:: start(velocity: int = 32767) -> None

    Generate a (re-) start event. Velocity is optional and must be within 1..32767. Undefined if used with a non-trigger type signal.
    Typically, velocity is interpreted as volume, but neither senders nor receivers need to honour this.

.. py:method:: stop() -> None

    Generate a stop event. Undefined if used with a non-trigger type signal.

.. warning::

    Processing trigger signals as audio or injecting other signal types is generally allowed, but we might have to switch encoding at some
    point, breaking such applications. We rather would just keep it but it has some disadvantages that we need to figure out better.

Normally you'd only connect trigger signals to other signal triggers or use them directly, but for completion's sake here is the encoding:
A change from 0 to any other value is a ``start`` event with ``abs(value)`` as a velocity parameter, a change from a nonzero value to a nonzero
value of opposite sign should be a ``restart`` event, and a change from a nonzero value to 0 is a ``stop event``. This means a 0-biased square wave
will continuously generate restart events. Right now there's a coding error in the trigger signals however so that giong from any nonzero value
to a different nonzero value will generate a ``restart`` event. Will get around to fixing that someday.

*This also illustrates why we like to not have trigger signals connected to nontrigger signals to be defined behavior yet: We did not have much
time for thinking or testing, and we feel like changing things up at this point is a big cost that requires a well though out solution, and
we're just not quite there yet.*


Lazy signals and constant buffers
---------------------------------

bl00mbox renders audio in 64 samples long chunks.

In principle, all signals in bl00mbox are intercompatible. However, it would often be a waste of CPU power to just to read/write a whole
48kHz sample rate buffer for, say, rarely modulating volume. bl00mbox signal buffers may carry a sentinel that indicates that the entire
64 samples long buffer is considered to be at the same value as its first element, allowing us to reduce memory access. Many plugins
have options to provide their output in constant buffer mode.

At 48kHz sample rate and 64 samples long buffer, the effective bandwidth of these signal channels is 375Hz with a sample period of 1.3ms,
so the time resolution is still reasonably high for rhythmic and modulation purposes.

Equally, inputs may be lazy and assume that they are fed by such a constant buffer. In most cases these just look at the first element of
the buffer, or, in case of trigger signals, compress the events within the buffer.

Dead ends
---------

bl00mbox does not render all plugins in an active channel at all times. A plugin is rendered exactly when another plugin uses it as a source.
For example:

.. code-block:: python

    blm.signals.line_in >> sampler.signals.record_input
    # this will never record anything as nobody requests data from the sampler.
    sampler.signals.record.trigger.start()

This behavior is intended as it can be used to efficiently shut down a whole network of plugins from a single plugin that is set to
mute for example, however as shown above it sometimes can have unintended side effects. If we do want to manually enable this mode
we can tell the backend to always render the plugin:

.. code-block:: python

    sampler.always_render = True

Circular connections
--------------------

No plugin is rendered more than once per cycle. We can use this to implement circular connections:

.. code-block:: python

    osc.signals.output >> range_shifter.signals.input
    range_shifter.signals.output_range[0].value = -10
    range_shifter.signals.output_range[1].value = 10
    osc.signals.fm << range_shifter.signals.output

Circular dependencies are generally allowed, but they do come with a grain of salt: If the rendering engine finds itself at the
situation where during the render of a plugin data from the same plugin is being requested, it simply uses data from the last render
cycle. This of course means that there is suddenly a buffer-length delay (i.e., 4/3 ms) in the feedback loop which means the
feedback-fm topology above has a pitch-dependent waveform, but it's better than not having the feature at all so we just went for
it :P.

