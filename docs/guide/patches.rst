Patches
=======

A patch is a group of plugins in a trench coat that look like a plugin. This has 2 major use cases:

- **For the casual user:** Making advanced sounds accessible easily by providing a big patch library
- **For the sound designer:** Manage high level plugin networks conveniently and define a stable
  interface to be able to update their inner workings without breaking API

Using the patch library
-----------------------

We are at this point very much failing at the patch library. It's in the ``bl00mbox.patches`` section.
It ain't much, but we now have some infrastructure to finally start expanding it.

In general, patches behave just like plugins from the outside: They are created by ``Channel.new`` and
provide a ``.signals`` container alongside optional other attributes and methods:

.. code-block:: python

    import bl00mbox

    blm = bl00mbox.Channel("PatchExample")
    # create a patch as if it was a plugin
    tinysynth = blm.new(bl00mbox.patches.tinysynth)
    # signals work just as with plugins too
    blm.signals.line_out << tinysynth.signals.output
    tinysynth.signals.sustain = 0
    tinysynth.signals.trigger.start()

    # you can delete an entire patch worth of plugins!
    tinysynth.delete()

For flow3r purposes, you may search for applications that contain the string ``bl00mbox.Patch`` and you
may find a few enjoyable ones to copy-paste into your application!

.. warning::

    You can do a lot by accessing unexposed surfaces of a provided patch, but you risk your application breaking in with
    future updates. If you like a patch and want to expand it, simply it into your project so that future changes of the
    internals do not affect you.

    Also, the API for writing patches underwent a few changes, for example there used to be a ``.plugins`` member that
    you'd add plugins to manually; please do not use this API for new patches and stick to the pattern below.


Creating new patches
--------------------

A patch is a group of plugins that expose only a stable subset of their signals to the outside, which is convenient
if you want to handle a bunch of them. Say we take the example from the ``Bleepy`` section and wanna play more than
one note. Of course, we could put everything in lists and wrap things in a loop, but there's a neater way:

.. code-block:: python

    # we inherit from bl00mbox.Patch to include its functionality
    class SimpleSynth(bl00mbox.Patch):
        def __init__(self, chan):
            # we must call the original initializer of bl00mbox.Patch here
            super().__init__(chan)

            # Patch features a .new method, just like bl00mbox.Channel.
            # super().__init__(chan) set up a weak reference to the channel
            # and Patch wraps around it
            osc = self.new(bl00mbox.plugins.osc)
            env = self.new(bl00mbox.plugins.env_adsr)

            osc.signals.output >> env.signals.input

            # whatever signals should be accessible from the outside
            # are stored in the .signals container of the Patch
            self.signals.trigger = env.signals.trigger
            self.signals.output = env.signals.output
            self.signals.pitch = osc.signals.pitch

We can now use the ``SimpleSynth`` class as if it were a plugin:

.. code-block:: python

    num_voices = 3
    mixer = blm.new(mixer, num_voices)
    mixer.signals.output >> blm.signals.line_out

    synths = []
    for x in range(num_voices)
        synth = blm.new(SimpleSynth)
        mixer.signals.input[x] << synth.signals.output
        synth.signals.pitch.tone = x * 2
        synths.append(synth)

    import time
    for synth in synths:
        synth.signals.trigger.start()
        time.sleep_ms(200)
    for synth in synths:
        synth.signals.trigger.stop()
        time.sleep_ms(200)

Exposing features
^^^^^^^^^^^^^^^^^

Note that from the outside we are never accessing the plugin objects directly. All signals that were not exposed are
hidden from us. Not that this isn't hackable, but we're doing this for a good reason: We may change internally how
a plugin works, expand what it can do, even silently replace it with a standalone plugin that does the job more
efficiently.

If you need references to plugins or signals for using them in methods or properties, seperately store them somewhere
nonpublic (i.e., underscore-prefixed):

.. code-block:: python

    class NoiseSynth(bl00mbox.Patch):
        def __init__(self, chan):
            super().__init__(chan)

            # as before
            osc = self.new(bl00mbox.plugins.osc)
            env = self.new(bl00mbox.plugins.env_adsr)

            self.signals.trigger = env.signals.trigger
            self.signals.output = env.signals.output
            self.signals.pitch = osc.signals.pitch

            # let's make things noisy
            nb = self.new(bl00mbox.plugins.noise_burst)
            nb.signals.length.value = 0
            nb.signals.trigger << osc.signals.sync_output

            # save signals to switch them around later
            self._nb_output = nb.signals.output
            self._osc_output = osc.signals.output
            self._env_input = env.signals.input
    
            # initialize connection
            self.set_noise(False)

        def set_noise(self, noise_on):
            '''
            Call this method with True to enable noise mode or False to disable it
            '''
            if noise_on:
                self._env_input << self._nb_output
            else:
                self._env_input << self._osc_output


Signal management
^^^^^^^^^^^^^^^^^

Patch signals should be stored somewhere in the ``.signals`` attribute of a patch instance, but they may be contained
in intermediate attributes, such as tuples. There are no standard objects provided yet, but you can copy them into
your project for now:

.. code-block:: python

    class SignalTuple(tuple):
        pass

    class SignalContainer:
        def copy_from(self, container, names):
            '''
            Takes list of attribute names and copies their references
            from container to self under the same name.
            '''
            for name in names:
                setattr(self, name, getattr(container, name))

This allows you to efficiently group signals. Let's take it for a spin:

.. code-block:: python

    # we inherit from bl00mbox.Patch to include its functionality
    class OctaveSynth(bl00mbox.Patch):
        def __init__(self, chan, num_oscs = 3):
            super().__init__(chan)
            mp = self.new(bl00mbox.plugins.multipitch, num_oscs)
            mixer = self.new(bl00mbox.plugins.mixer, num_oscs)
            env = self.new(bl00mbox.plugins.env_adsr)
            mixer.signals.output >> env.signals.input

            self.signals.trigger = env.signals.trigger
            self.signals.output = env.signals.output
            self.signals.pitch = mp.signals.pitch
            self.signals.gain = mixer.signals.gain

            # create a container within .signals
            # and copy over a bunch of env signals
            self.signals.env = SignalContainer()
            self.signals.env.copy_from(env.signals, ["attack", "release", "decay", "sustain"])

            # for the oscillators we'll first collect
            # SignalContainers for the individual plugins:
            osc_sigs = []
            for x in range(num_oscs):
                osc = self.new(bl00mbox.plugins.osc)
                osc.signals.output >> mixer.signals.input[x]
                osc.signals.pitch << mp.signals.output[x]
                mp.signals.shift[x].tone = 12 * x
                sigs = SignalContainer()
                # as before, we copy a bunch from the plugin
                sigs.copy_from(osc.signals, ["waveform", "morph", "fm"])
                # but we also add them from somewhere else
                sigs.shift = mp.signals.shift[x]
                sigs.gain = mixer.signals.input_gain[x]
                osc_sigs.append(sigs)
            # and then expose them as a tuple here
            self.signals.osc = SignalTuple(osc_sigs)

Note how the ``osc`` attribute provides convenient access to parameters that are relevant to the
oscillator but stored in a different plugin: We've "disentangled" the ``.input_gain[]`` signal
tuple provided by the ``mixer`` plugin and distributed them over the oscillators they are associated
with. This  makes for a very clean user interface:

.. code-block:: python

    synth = blm.new(OctaveSynth)

    # change global envelope sustain
    synth.signals.env.sustain.value = 0
    # change waveform of lowest osc
    synth.signals.osc[0].waveform.value = 0
    # change pitch of highest osc
    synth.signals.osc[2].shift.tone = 17
    # reduce volume of middle osc
    synth.signals.osc[1].gain.dB -= 3

Standard interfaces and patch swapping
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are no formally defined interfaces as of yet. We're working on some sort of formal interface tagging
system but we haven't figured out a solution we like yet.

So far, we've been using two de-facto standards for synthesizers and effects:

- For tonal synths we ususally expose ``.output``, ``.trigger`` and ``.pitch``.
- For effects, we have exposed a simple ``.input``/``.output`` interface.

These signals come with expected behaviors, ``.trigger`` of a synth shouldn't be an output for example. Since
there's no formal standard yet there's no use in overspecifying it here.

You can still already use these standard interfaces! Say we want to make a patch that creates an instance of
an effect but with added wet/dry mixing options if it doesn't have it already, we could do so like this:

.. code-block:: python

    class WetDryEffect(bl00mbox.Patch):
        '''
        Takes an effect patch and warps wet/dry mixer around it.
        The patch instance is exposed in .effect.
        The added mixer signals are exposed in .signals.[wet/dry/gain].
        *args/**kwargs are forwarded to the effect patch initializer.
        Inner effect can be swapped at runtime with .set_effect().
        '''
        def __init__(self, blm, effect_patch, \*args, \*\*kwargs):
            super().__init__(self, blm)
            self._buffer = self.new(bl00mbox.plugins.buffer)
            self._mixer = self.new(bl00mbox.plugins.mixer, 2)
            self.effect = None
            self.set_effect(effect_patch, \*args, \*\*kwargs)
            self._buffer.signals.output >> self._mixer.signals.input[0]
            self._mixer.signals.input_gain[0].mult = 0.5
            self._mixer.signals.input_gain[1].mult = 0.5
            self._mixer.signals.gain.mult = 1
            self.signals.output = mixer.signals.output
            self.signals.input = buffer.signals.input
            self.signals.wet = mixer.signals.input_gain[0]
            self.signals.dry = mixer.signals.input_gain[1]
            self.signals.gain = mixer.signals.gain

        def set_effect(self, effect_patch, \*args, \*\*kwargs):
            effect = self.new(bl00mbox.plugins.effects_patch, \*args, \*\*kwargs)
            # tip: connect input first to avoid "blips"
            effect.signals.input << self._buffer.signals.output
            effect.signals.output >> self._mixer.signals.input[1]

            if self.effect is not None:
                self.effect.delete()
            self.effect = effect

Note how we reimplemented the effect interface from above again, so while the signal internals are different
this still "looks" like an effect, we have just moved signals around. One interesting side effect is that this
allows us to swap the internal effect with the ``.set_effect()`` method in a running system without manually
reconnecting the interface signals.

The inner effect is exposed in its entirety as the ``.effect`` member. Remember that usually we don't expose
internal plugins/patches to stay flexible, but here it makes a lot of sense: While we could just simply copy
its signals like ``self.signals.fx = effect.signals``, the inner effect might also implement methods that the
user wants to access. Taking a guess and copying a set of random additional ``effect`` members is tedious and
error-prone, so it's best to just expose the entire object.

This swapping doesn't always work seamlessly; let's consider a patch that takes a synth style patch, creates a
number of copies and connects them all to a shared mixer, exposes their triggers and pitch signals and tries to
do the same:

.. code-block:: python

    class BrokenTinyPolySynth(bl00mbox.Patch):
        '''
        Takes a synth patch, creates num_voices instances of it and connects
        them to a mixer.
        The synth instances are exposed in .synths[].
        The mixer output is exposed in .signals.output.
        Triggers/pitches of the synths are exposed in .signals.[pitch/trigger][]
        Synths can be swapped at runtime with .set_synth() but the connections
        to their pitch and trigger inputs are not retained.
        '''
        def __init__(self, blm, synth_patch, num_voices, \*args, \*\*kwargs):
            super().__init__(self, blm)
            self._num_voices = num_voices
            self._mixer = self.new(bl00mbox.plugins.mixer, num_voices)
            self.synths = []
            self.set_synth(synth_patch, \*args, \*\*kwargs)
            self.signals.output = mixer.signals.output

        def set_synth(self, synth_patch, \*args, \*\*kwargs):
            synths = []
            triggers = []
            pitches = []
            for x in range(self._num_voices):
                synth = self.new(bl00mbox.plugins.synth_patch, \*args, \*\*kwargs)
                synth.signals.output >> self._mixer.signals.input[x]
                synths.append(synth)
                triggers.append(synth.signals.trigger)
                pitches.append(synth.signals.trigger)
            self.signals.pitch = SignalTuple(pitches)
            self.signals.trigger = SignalTuple(trigger)

            for synth in self.synths:
                synth.delete()
            self.synths = tuple(synths)

If we hook up this patch to some external system and then call ``set_synth`` with another patch, all signals
in ``trigger`` and ``pitch`` are destroyed and recreated and therefore lose their connections. bl00mbox has
no canonical way to retrieve a list of signals that another signal is connected to at this point, so we can't
really redo them without knowning what they were.

A band-aid is provided by the ``buffer`` plugin: It can generate an intermediate signal that behaves independently
and is not disconnected, but currently it does live in the render tree and does consume a bit of CPU. We intend to
clean these general signal issues up soon, but for now let's just accept a bit of performance loss and add buffers
in-between everything:

.. code-block:: python

    class TinyPolySynth(bl00mbox.Patch):
        '''
        Takes a synth patch, creates num_voices instances of it and connects
        them to a mixer.
        The synth instances are exposed in .synths[].
        The mixer output is exposed in .signals.output.
        Triggers/pitches of the synths are exposed in .signals.[pitch/trigger][]
        Synths can be swapped at runtime with .set_synth().
        '''
        def __init__(self, blm, synth_patch, num_voices, \*args, \*\*kwargs):
            super().__init__(self, blm)
            self._num_voices = num_voices
            self._mixer = self.new(bl00mbox.plugins.mixer, num_voices)
            self._trigger_buffers = [self.new(bl00mbox.plugins.buffer for _ in range(num_voices)]
            self._pitch_buffers = [self.new(bl00mbox.plugins.buffer for _ in range(num_voices)]

            self.signals.pitch = SignalTuple([b.signals.input for b in self._pitch_buffers])
            self.signals.trigger = SignalTuple([b.signals.input for b in self._trigger_buffers])
            self.signals.output = mixer.signals.output

            self.synths = []
            self.set_synth(synth_patch, \*args, \*\*kwargs)

        def set_synth(self, synth_patch, \*args, \*\*kwargs):
            synths = []
            for x in range(self._num_voices):
                synth = self.new(bl00mbox.plugins.synth_patch, \*args, \*\*kwargs)
                synth.signals.output >> self._mixer.signals.input[x]
                synth.signals.pitch << self._pitch_buffers[x].signals.output
                synth.signals.trigger << self._trigger_buffers[x].signals.output
                synths.append(synth)

            for synth in self._synths:
                synth.delete()
            self.synths = synths


Note how this patch looks almost like a synth patch as described above, but ``pitch`` and ``trigger`` are tuples.
This serves as a good example why coming up with a good interface tagging system is difficult.

Let's continue with the example above. Say you have it initialized with a specific synth that has a parameter,
and you'd like to change that parameter across all instances at the same time. We can use a ``buffer`` to create
a helper function as such:

.. code-block:: python

    class SmallPolySynth(TinyPolySynth):
        def set_synth(self, synth_patch, *args, **kwargs):
            super().set_synth(synth_patch, *args, **kwargs)
            # these are synth patch specific so we should clear them
            self.signals.synth = SignalContainer()

        def set_common_param(self, source_name, target_name):
            common = self.new(bl00mbox.plugins.buffer)
            for synth in self.synths:
                signal = getattr(synth.signals, source_name)
                common.signals.output >> signal
            setattr(self.signals.synth, target_name)
            
We could use it like so for example:

.. code-block:: python

    poly = blm.new(SmallPolySynth(bl00mbox.patches.Tinysynth, 5))
    poly.set_common_param("sustain", "env_sustain")
    # turn off sustain for all at the same time
    poly.signals.synth.env_sustain.value = 0

Note that as soon as a user writes a value to a the sustain signal of a specific instance this connection is severed.
Also this doesn't account for signals that are not directly attributes of ``.signals``. We aim to provide ``SignalContainer``
/``SignalTuple`` specific replacement methods for ``getattr``/``setattr`` someday soon.
