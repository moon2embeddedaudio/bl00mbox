Examples
========

How to play along
-----------------

Dependencies
^^^^^^^^^^^^

If you wanna play along with these examples, go into the REPL of your bl00mbox device and initialize it like this:

.. code-block:: python

    import time, math, random
    import bl00mbox

    blm = bl00mbox.Channel("examples")

Each individual section may have additional requirements, so if you wanna jump to a subsection make sure to check
its parents for potential dependencies.

REPL pasting
^^^^^^^^^^^^

Once your dependencies are set up you can then copy-paste examples into the REPL. There's two pitfalls with that:

- Typical setups have a limited buffer size, so you might not be able to paste everything at the same time.

- The REPL is sensitive to whitespace as you'd expect from python, but it also dislikes empty newlines within
  function or class definitions, so make sure you copy the exact block you want.

.. note::

    Depending on your system, highlighting a code snippet in these examples starting from top or bottom may
    have different whitespace behavior.


This is not shiny but works well enough usually. If you do have issues with it, the other way would be to
write all classes and other dependencies into a ``.py`` file on your micropython host and then importing it.

*Since the REPL does not allow for newlines within a method definitions we're using empty comments*
(``#``) *instead in the following examples. It is not recommended for production code, we just do it
in this page for the significant convenience it offers.*


Modifying examples
^^^^^^^^^^^^^^^^^^

.. note::

    bl00mbox is very sensitive to small changes, so if you want to follow along with the text and have modified
    any state that is still in use, please undo your changes or create the environment from scratch to make sure
    the examples work as intended.

If you want to change up the examples, you can often do many things directly in the REPL, such as rerouting
connections, setting signal values and creating new plugins.

If you want to modify one of the patch classes shown here we recommend copying them into an intermediate text
editor and pasting the entire thing into the REPL again after making changes, we find it to be the fastest way.

.. warning::

    When playing with examples in the REPL avoid disconnecting from output-type signals, i.e.
    ``signal_tx >> None``, because it is difficult to restore: It may disconnect much more connections than you
    intended, see the :ref:`Signals & Connections<sigconn-disconnect>` section.

Make the sound stop
^^^^^^^^^^^^^^^^^^^

Many of these examples are set to autoplay so you can mess with parameters. Here's how to stop it:

.. code-block:: python

    blm.foreground = False

This method has the advantage that as soon as you start interacting with the channel again it comes
back to life automatically. This may also be annoying for some purposes and we will add an option
to turn this feature off soon. If you wish to avoid it right now you can set the channel volume:

.. code-block:: python

    # store last nonmuted volume, -12 is default for new channels
    _blm_gain_dB = -12

    def mute(unmute = False)
        if blm.gain_dB != -math.inf:
            _blm_gain_dB = blm.gain_dB
            blm.gain_dB = -math.inf

    def unmute():
        blm.gain_dB = _blm_gain_dB

Patterns
--------

Let's look into how to make bl00mbox play patterns. To have something to play patterns with, let's
get a simple synthesizer and hook it up to the output:

.. code-block:: python

    tinysynth = blm.new(bl00mbox.patches.tinysynth)
    tinysynth.signals.output >> blm.signals.line_out
    tinysynth.signals.sustain.value = 0

    # reminder: we can play it directly like so:
    for x in range(10):
        tinysynth.signals.pitch.tone = -24 + x * 2
        tinysynth.signals.trigger.start()
        time.sleep_ms(200)

Periodic Triggers
^^^^^^^^^^^^^^^^^

Let's start with the simplest case and trigger a note periodically. The ``osc`` plugin provides a
``sync_output`` trigger signal that we can use for this purpose:

.. code-block:: python

    periodic_trigger = blm.new(bl00mbox.plugins.osc)
    periodic_trigger.signals.pitch.freq = 0.5
    periodic_trigger.signals.sync_output >> tinysynth.signals.trigger

A note should now play every 2 seconds (freq = 0.5Hz). This gets old very fast. To turn it off we can
of course simply disable channel rendering.

.. code-block:: python
    
    # Stop sound
    blm.foreground = False
    # Restart sound. This also happens automatically when interacting with the channel.
    blm.foreground = True

To make things more interesting, let's play a random note every time a new trigger arrives. We can hook
it up like so:

.. code-block:: python
    
    pitch_randomizer = blm.new(bl00mbox.plugins.noise_burst)
    pitch_randomizer.signals.length.value = 0
    pitch_range = blm.new(bl00mbox.plugins.range_shifter)
    pitch_range.signals.output_range[0].tone = -24
    pitch_range.signals.output_range[1].tone = 0
    pitch_randomizer.signals.output >> pitch_range.signals.input
    pitch_range.signals.output >> tinysynth.signals.pitch

    periodic_trigger.signals.sync_output >> pitch_randomizer.signals.trigger

.. _examples-patterns-hard-sync:

Hard sync
^^^^^^^^^

*This builds up on the previous subsection and requires its REPL state*

A useful technique for creating rhythms is hard synchronization: This means to simply reset an oscillator
to its starting point when a trigger arrives. This can be used on the waveform-level for audio synthesis,
but in the rhythmic domain we could use it for example to generate a clave like this:

.. code-block:: python

    clave_trigger = blm.new(bl00mbox.plugins.osc)
    clave_trigger.signals.pitch.freq = 0.5 * 8/3
    clave_trigger.signals.sync_input << periodic_trigger.signals.sync_output
    clave_trigger.signals.sync_output >> tinysynth.signals.trigger

The ``sync_input_phase`` parameter determines to which value the ``clave_trigger`` oscillator gets reset,
i.e. how long it will take until it sends its next trigger event. We can mess around with it to create
an offset rhythm:

.. code-block:: python

    clave_trigger.signals.sync_input_phase.value = 12000

Lastly, we manually matched the frequencies of the two oscillators (one is set to 0.5Hz, the other to
an ominous (0.5 * 8/3)Hz. If we want to speed up or slow down this rhythm, we have to keep both in sync.
To automate this, we can use the ``multipitch`` plugin to create two signals with a fixed ratio:

.. code-block:: python

    clave_sync = blm.new(bl00mbox.plugins.multipitch, 1)
    clave_sync.signals.thru >> periodic_trigger.signals.pitch
    clave_sync.signals.output[0] >> clave_trigger.signals.pitch
    clave_sync.signals.shift[0].freq *= 8/3
    clave_sync.signals.input.freq = 0.5
    # important: we must disable the min pitch feature by setting
    # it to its minimum
    clave_sync.signals.min_pitch.value = -32767


Now we can freely change the speed of the rhtyhm as a whole:

.. code-block:: python

    clave_sync.signals.input.freq = 1

You might have wondered why we decided to use the same signal type for event rate and tonal pitch. There
is an inherent duality between rhythm and tone. If you wish to see for yourself, try speeding up the
rhythm up into into the tonal range, say, concert A at 440Hz:

.. code-block:: python
    
    # cool noise
    clave_sync.signals.input.freq = 440

Value modulation
^^^^^^^^^^^^^^^^

*This builds up on the previous subsection and requires its REPL state*

So far we were mostly concerned with trigger signals. Let's use our new found noise mode and modulate into
it. Once again we use an ``osc`` plugin, but this time we're using the main output instead of the sync
sidechannel. To modulate between the two values above, we set up a square wave and morph it so that 
it spends more time in the "regular" mode than in the "noise" mode:

.. code-block:: python

    noise_osc = blm.new(bl00mbox.plugins.osc)
    noise_osc.signals.waveform.switch.SQUARE = True
    noise_osc.signals.morph.value = 18000
    noise_osc.signals.pitch.freq = 0.3

    noise_range = blm.new(bl00mbox.plugins.range_shifter)
    noise_range.signals.output_range[0].freq = 1
    noise_range.signals.output_range[1].freq = 440
    noise_osc.signals.output >> noise_range.signals.input
    noise_range.signals.output >> clave_sync.signals.input

We can of course play around with different waveforms to get different effects. Say we want a tremolo that
is active only when we're in the "noise" mode. Let's borrow the ``Tremolo`` patch from :ref:`below<examples-effects-tremolo>`
and hook it up:

.. code-block:: python

    # (copy-paste Tremolo patch in here)

    tremolo = blm.new(Tremolo)
    tinysynth.signals.output >> tremolo.signals.input
    tremolo.signals.output >> blm.signals.line_out
    tremolo_range = blm.new(bl00mbox.plugins.range_shifter)
    tremolo.signals.depth << tremolo_range.signals.output
    tremolo_range.signals.output_range[0].value = 0
    tremolo_range.signals.output_range[1].value = 32767
    noise_osc.signals.output >> tremolo_range.signals.input

.. _examples-patterns-random-trigger:

Random triggers
^^^^^^^^^^^^^^^

Often you'd want your triggers to occur at random intervals. We can use a similar trick as before with
the random notes, but this time randomize the frequency of an ``osc`` plugin each time it sends a sync
signal. Let's use a patch to package it nicely:

.. code-block:: python

    class RandomPitchTrigger(bl00mbox.Patch):
        def __init__(self, chan):
            super().__init__(chan)
            osc = self.new(bl00mbox.plugins.osc)
            rng = self.new(bl00mbox.plugins.noise_burst)
            speed_range = self.new(bl00mbox.plugins.range_shifter)
            rng.signals.length.value = 0
            speed_range.speed = "slow"
            osc.speed = "lfo"
            speed_range.signals.output_range[0].freq = 0.3
            speed_range.signals.output_range[1].freq = 3
            # 
            # closed ring of 3 plugins, no problem
            osc.signals.sync_output >> rng.signals.trigger
            rng.signals.output >> speed_range.signals.input
            speed_range.signals.output >> osc.signals.pitch 
            #
            # we'll take both of the range signals
            self.signals.speed_range = speed_range.signals.output_range
            self.signals.trigger_out = osc.signals.sync_output
            #
            # add an independent pitch output by the same pattern
            pitch_rng = self.new(bl00mbox.plugins.noise_burst)
            pitch_range = self.new(bl00mbox.plugins.range_shifter)
            pitch_rng.signals.length.value = 0
            osc.signals.sync_output >> pitch_rng.signals.trigger
            pitch_rng.signals.output >> pitch_range.signals.input
            #
            pitch_range.signals.output_range[0].freq = 40
            pitch_range.signals.output_range[1].freq = 300
            #
            self.signals.pitch_range = pitch_range.signals.output_range
            self.signals.pitch_out = pitch_range.signals.output

Trigger Sequencing
^^^^^^^^^^^^^^^^^^

*This builds up on the previous subsection and requires its REPL state*

Of course we can also provide a sequence of triggers, pitches or other values and play through it.
We provide the ``sequencer`` plugin for that purpose. Let's clean up tinysynth a bit and try it:

.. code-block:: python

    tinysynth.signals.pitch.tone = -12
    tinysynth.signals.output >> blm.signals.line_out

    sequencer = blm.new(bl00mbox.plugins.sequencer, 2, 16)

    sequencer.trigger_start(0, 0)
    sequencer.trigger_start(0, 2)
    sequencer.trigger_start(0, 7)
    sequencer.trigger_start(0, 8)

    tinysynth.signals.trigger = sequencer.signals.track[0]

We hear a sequence, but some of the attacks get swallowed. This is due to tinysynth's sluggish envelope,
let's speed it up a bit:

.. code-block:: python

    tinysynth.signals.decay.value = 200
    tinysynth.signals.attack.value = 20

Ah, that's better. But what if we want variable length notes? Well, we could modulate the decay and attack
parameters as above, but let's try a different approach: The ``sync_output`` of the oscillator plugins
only ever send ``start`` events, but ``stop`` events exist as well. We have set up our tinysynth in the very
beginning to stop playing automatically by setting the ``sustain`` signal to zero, but let's undo that and
add explicit ``stop`` markers:

.. code-block:: python

    sequencer.trigger_stop(0, 9)
    tinysynth.signals.sustain.value = 16000

Value Sequencing
^^^^^^^^^^^^^^^^

Unfortunately, this API is very underdeveloped, see the documentation of the ``sequencer`` plugin. We can
still use it with the ``sequencer_value_*`` functions from there and the ``tone_to_sct`` function documented in the
``bl00mbox.helpers`` module. These will be properly integrated in the modules soon, but for now here for copypasting:

.. code-block:: python

    SCT_A440 = 18367
    SCT_OCTAVE = 2400

    def tone_to_sct(tone: float):
        return (tone * SCT_OCTAVE) // 12 + SCT_A440

    def sequencer_value_set_mode(seq, track, on = True):
        table = seq.table_int16_array
        flag_index = seq._get_table_index(track, -1)
        table[flag_index] = 32767 if on else -32767


    def sequencer_value_set(seq, track, step, value):
        table = seq.table_int16_array
        step_index = seq._get_table_index(track, step)
        table[step_index] = value

Let's build a little wrapper for our purposes around that. Note that we need to fill every single step of the
sequencer track as they all got initialized to 0:

.. code-block:: python

    melody = [2,2,5,5,7,7,0,0,10,10,9,9,7,7,6,6]

    def play_melody(sequencer, track, melody):
        sequencer_value_set_mode(sequencer, track)
        for x, note in enumerate(melody):
            sequencer_value_set(sequencer, track, x, tone_to_sct(note))

    play_melody(sequencer, 1, melody)
    tinysynth.signals.pitch << sequencer.signals.track[1]

A lot of the notes go unheard since they are barred behind a ``stop`` signal, but rest assured that they
are forwarded to the pitch input of the synthesizer. Let's make them audible with a really long release:

.. code-block:: python

    tinysynth.signals.release.value = 1000

We can also add a volume value to the ``start`` events. Let's give the very last note its own envelope
but at much reduced volume:

.. code-block:: python

    sequencer.trigger_start(0, 15, 10000)

Since the ``sequencer`` plugin has remained nearly unchanged since the very early days it doesn't follow
any particular timing standard, sadly. We can still make them play together though! Let's add a second
one that shifts the entire pitch output of the first:

.. code-block:: python

    pitch_shifter = blm.new(bl00mbox.plugins.multipitch, 1)
    sequencer.signals.track[1] >> pitch_shifter.signals.input
    pitch_shifter.signals.output[0] >> tinysynth.signals.pitch

    pitch_shift_sequencer = blm.new(bl00mbox.plugins.sequencer,1,4)
    # goes 1/16th as fast
    pitch_shift_sequencer.signals.beat_div.value = 1
    # set up a melody
    play_melody(pitch_shift_sequencer, 0, [0,-5,-7,-4])
    # modulate pitch with it
    pitch_shift_sequencer.signals.track[0] >> pitch_shift.signals.shift[0]

    # since the sync_out signals is broken right now, we sync both to an oscillator:
    sync_osc = blm.new(bl00mbox.plugins.osc)
    sync_osc.signals.sync_output >> sequencer.signals.sync_in
    sync_osc.signals.sync_output >> pitch_shift_sequencer.signals.sync_in
    # magic number
    sync_osc.signals.pitch.freq = 0.125

Here we see the lack of standard signals in the sequencer plugin fall apart. We hope we can provide a better
approach here soon!

Tonal Synths
------------

Here's some synths that can produce more-or-less pitched sounds to play melodies, chords and thelikes
with. Let's set up a testing environment with ``RandomPitchTrigger`` from the section
:ref:`above<examples-patterns-random-trigger>`:

.. code-block:: python

    autoplay = blm.new(RandomPitchTrigger)

    def test_melody(synth):
        synth.signals.output >> blm.signals.line_out
        synth.signals.trigger << autoplay.signals.trigger_out
        synth.signals.pitch << autoplay.signals.pitch_out
        return synth

    # we can now quickly test patches:
    synth = test_melody(blm.new(bl00mbox.patches.tinysynth))
    # we're holding on to the synth handle to
    # prevent it from being garbage collected

Also we'll add a class that bl00mbox really should just come with. We'll use it to more efficiently
copy signals from contained plugins:

.. code-block:: python

    class SignalTuple(tuple):
        pass

    class SignalContainer:
        def copy_from(self, container, names):
            for name in names:
                setattr(self, name, getattr(container, name))

Minimal synth setup
^^^^^^^^^^^^^^^^^^^

A very simple synthesizer would be an oscillator to generate continouos sound into a envelope generator
to go through a volume curve. Let's build a simple one from scratch in the REPL:

.. code-block:: python

    osc = blm.new(bl00mbox.plugins.osc)
    env = blm.new(bl00mbox.plugins.env_adsr)
    osc.signals.output >> env.signals.input

This is pretty much just like the tinysynth patch we used for testing our helper functions. However, we
can't use the ``test_melody()`` function from above just yet since we don't provide a single surface
with all the signals, so let's connect it manually for starters:

.. code-block:: python

    env.signals.trigger << autoplay.signals.trigger_out
    osc.signals.pitch << autoplay.signals.pitch_out
    env.signals.output >> blm.signals.line_out

We can now play around with parameters a bit: 

.. code-block:: python
    
    # let's try a square (reduce volume a bit)
    env.signals.gain.dB -= 6
    osc.signals.waveform.switch.SQUARE = True
    # with slow decay/release
    env.signals.decay.value = 1300
    env.signals.release.value = 1300
    # that automatically turns off
    env.signals.sustain.value = 0

Let's add a common feature and modulate a filter with the envelope. Let's hook one up:

.. code-block:: python

    filt = blm.new(bl00mbox.plugins.filter)
    filt.signals.output >> blm.signals.line_out
    filt.signals.input << env.signals.output
    filt.signals.cutoff.freq = 5000

The output provides the ``.env_output`` signal which represents its gain (kinda, see plugin docs). If you
were to just plugin that into the ``.cutoff`` signal you'd see that they are very incompatible and we need
to adapt the range. We can use a ``range_shifter`` plugin for that:

.. code-block:: python

    env_to_pitch = blm.new(bl00mbox.plugins.range_shifter)
    # input is range of the .env_output signal
    env_to_pitch.signals.input_range[0].mult = 0
    env_to_pitch.signals.input_range[1].mult = 1
    env_to_pitch.signals.input << env.signals.env_output
    # output is the cutoff range we want
    env_to_pitch.signals.output_range[0].freq = 200
    env_to_pitch.signals.output_range[1].freq = 2000
    env_to_pitch.signals.output >> filt.signals.cutoff

If we want to get rid of that automation again we can simply disconnect to reset to the 5kHz we set earlier:

.. code-block:: python

    filt.signals.cutoff.freq << None

Modulating oscillators
^^^^^^^^^^^^^^^^^^^^^^

*This builds up on the previous subsection and requires its REPL state*

That's all fair and well but not too exciting. Let's go a little wilder:

.. code-block:: python

    osc.signals.morph << env.signals.output

That is definitely more interesting, but a bit on the harsh side. Let's use a filter to mellow it out a bit:

.. code-block:: python

    filt = blm.new(bl00mbox.plugins.filter)
    filt.signals.output >> blm.signals.line_out
    filt.signals.input << env.signals.output
    filt.signals.cutoff.freq = 5000

Note how we're using the *audio* output, not the *control signal* output ``.env_output`` for modulation.
The waveform input is not lazy, meaning for each single audio sample the ``waveform`` parameter is
updated to the value of the audio output of the synthesizer. This has some major side effects:

We used the audio output of the *envelope generator* instead of the *oscillator*, meaning that the amount
of modulation is dependent on the current output level. This means different stages in the envelope curve
correspond to different waveforms as well, which helps a lot in making things sound a bit more lively!

We also created a closed loop, so bl00mbox introduces a delay of *1 buffer / 64 samples / 750 Hz*
into the loop signal. We cannot audibly perceive the delay as such, but if you think of that delay relative
to the pitch you'll see that it introduces a *pitch-dependent phase shift* to the signal. In short, different
pitches correspond to different waveforms, which helps greatly in making things sound more lively!

One more thing: Earlier when we first switched to the square wave option we reduced sound level a bit with
the ``gain`` signal to even out the difference to the triangle waveform. Since this is also in the path of
modulation this has an effect. Let's make those parameters independent by introducing an entire own
envelope for the modulation sidechain:

.. code-block:: python

    # sandwich envelope in the modulation path
    mod_env = blm.new(bl00mbox.plugins.env_adsr)
    osc.signals.morph << mod_env.signals.output
    mod_env.signals.input << osc.signals.output

    # hook up to same trigger source as main envelope
    mod_env.signals.trigger << autoplay.signals.trigger_out

    # set up individual envelope parameters
    mod_env.signals.sustain.value = 0
    mod_env.signals.attack.value = 150
    mod_env.signals.decay.value = 500
    mod_env.signals.release.value = 500

    # restore our default modulation depth
    mod_env.signals.gain.dB = -6

We can now control the amount of modulation easily. Let's try some settings:

.. code-block:: python
    
    # previously -6dB (.mult = 0.5)

    # reference: turn off modulation
    mod_env.signals.gain.mult = 0
    # strong modulation
    mod_env.signals.gain.mult = 1
    # subtle modulation
    mod_env.signals.gain.mult = 0.25
    # extreme modulation (potentially a bug?)
    mod_env.signals.gain.mult = 8
    # restore
    mod_env.signals.gain.mult = 0.5

This is neat but is not very descriptive. This is a good moment to switch to a patch to make things
more managable. Let's rebuild our synthesizer so far:

.. code-block:: python

    class MiniModSynth(bl00mbox.Patch):
        def __init__(self, blm):
            super().__init__(blm)
            # copy-pasted from above
            osc = self.new(bl00mbox.plugins.osc)
            env = self.new(bl00mbox.plugins.env_adsr)
            mod_env = self.new(bl00mbox.plugins.env_adsr)
            filt = self.new(bl00mbox.plugins.filter)
            filt.signals.output >> env.signals.input
            filt.signals.input << osc.signals.output
            osc.signals.morph << mod_env.signals.output
            mod_env.signals.input << osc.signals.output
            osc.signals.waveform.switch.SQUARE = True
            filt.signals.cutoff.freq = 5000
            # shorthand
            for e, a, d in ((env, 100, 1300), (mod_env, 150, 500)):
                e.signals.decay.value = d
                e.signals.release.value = d
                e.signals.sustain.value = 0
                e.signals.attack.value = a
            #
            filt.signals.gain.dB = -6
            mod_env.signals.gain.dB = -6
            # define outside-facing signals
            self.signals.output = env.signals.output
            self.signals.pitch = osc.signals.pitch
            self.signals.mod_depth = mod_env.signals.gain
            #
            # we need a common trigger input signal (see below)
            mp = self.new(bl00mbox.plugins.multipitch, 1)
            mp.signals.trigger_thru >> env.signals.trigger
            mp.signals.trigger_thru >> mod_env.signals.trigger
            self.signals.trigger = mp.signals.trigger_in

About ``self.signals.trigger``: If a user trivially connects to ``.signals.trigger``, they should by
default trigger both envelopes in sync as we did above in our test setup. To achieve this, we have
created intermediate signals provided by the ``multipich`` plugin, one input to expose to the outside,
and one output that replicates that input to an arbitrary amount of internal connections.

Also, we have moved the filter in front of the envelope. This has the advantage that when the envelope stops
the filter is not rendered anymore at all. However, we noticed an increased sharpness in sound when we tried
it. The ``osc`` plugin provides full range signal: In our old hookup, the envelope generator reduced gain
before the filter, which we flipped around with our change, the filter was simply clipping. We fixed this
easily by moving the gain reduction from the envelope generator to the filter.

We now have a patch that complies with the standard interface of ``test_melody()``, i.e. the ``.signals``
container has the right signals stored in the right attributes (``.output``, ``.pitch``, ``.trigger``).
We also expose the ``mod_osc.signals.gain`` signal that we used to change modulatio intensity and renamed
it to ``.mod_depth``. Let's try it out:

.. code-block:: python

    synth = test_melody(blm.new(MiniModSynth))
    for x in (0, 0.25, 0.5, 1, 8, 0.5):
        time.sleep_ms(2500)
        synth.signals.mod_depth.mult = x
        print("mod depth:", x)

Of course, we can use many other things as modulation sources; these may be in tune, such as our example here,
but you could also use noise or an unrelated pitch as a modulator to get more interesting textures -  you may
find it very noisy at first, but if you tweak it enough to hit just the right spots it livens up any sound!

In terms of modulation sinks the world is your oyster as well! Note that modulating lazy input signals with
audio band signals will result in artifacts that manifest as dysharmonic content; this can be mitigated
by filtering out high frequency content above 325Hz. You may also amplify it by doing the opposite, if
you're so inclined.

We've boldly modulated over a large range in these examples. With many signals you might want to modulate
over a much smaller (or non-centered) area. The ``range_shifter`` plugin is generally helpful for this purpose.

Managing multiple oscillators
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*This builds up on the previous subsection and requires its REPL state*

There is one issue: Because the internal plugins and their signals are considered unstable, we don't have
a good means of changing say waveform or envelope filters as they are not exposed. Let's fix this!

And while we're at it: Why have the modulation come from the same oscillator? We could maybe be more flexible
by having a secondary oscillator with its own waveform settings and all. We could even try different pitch
offsets and see what happens, our ``multipitch`` plugin provides this conveniently for us:

.. code-block:: python

    class ModSynth(bl00mbox.Patch):
        def __init__(self, blm):
            super().__init__(blm)
            # copy-pasted from above
            osc = self.new(bl00mbox.plugins.osc)
            env = self.new(bl00mbox.plugins.env_adsr)
            mod_env = self.new(bl00mbox.plugins.env_adsr)
            filt = self.new(bl00mbox.plugins.filter)
            filt = self._init_filter(env, osc)
            filt.signals.output >> env.signals.input
            filt.signals.input << osc.signals.output
            osc.signals.morph << mod_env.signals.output
            mod_env.signals.input << osc.signals.output
            osc.signals.waveform.switch.SQUARE = True
            for e, a, d in ((env, 100, 1300), (mod_env, 150, 500)):
                e.signals.decay.value = d
                e.signals.release.value = d
                e.signals.sustain.value = 0
                e.signals.attack.value = a
            #
            mod_env.signals.gain.dB = -6
            filt.signals.gain.dB = -6
            filt.signals.cutoff.freq = 5000
            mp = self.new(bl00mbox.plugins.multipitch, 1)
            mp.signals.trigger_thru >> env.signals.trigger
            mp.signals.trigger_thru >> mod_env.signals.trigger
            #
            # introduce mod_osc
            mod_osc = self.new(bl00mbox.plugins.osc)
            mod_env.signals.input << mod_osc.signals.output
            # replicate connection to simulate old behavior
            mod_env.signals.output >> mod_osc.signals.morph
            mod_osc.signals.waveform.switch.SQUARE = True
            # multipitch provides pitch offsets
            mp.signals.thru >> osc.signals.pitch
            mp.signals.output[0] >> mod_osc.signals.pitch
            # define signals as before
            self.signals.trigger = mp.signals.trigger_in
            self.signals.pitch = mp.signals.input
            self.signals.output = env.signals.output
            self.signals.gain = env.signals.gain
            # package other signals in descripitive containers:
            osc_signames = ["waveform", "morph"]
            env_signames = ["attack", "decay", "release", "sustain"]
            self.signals.osc = SignalContainer()
            self.signals.osc.copy_from(osc.signals, osc_signames + ["sync_output"])
            self.signals.env = SignalContainer()
            self.signals.env.copy_from(env.signals, env_signames)
            self.signals.filter = SignalContainer()
            self.signals.filter.copy_from(filt.signals, ["cutoff", "reso", "mix"])
            # we can also have containers within containers!
            self.signals.mod = SignalContainer()
            self.signals.mod.osc = SignalContainer()
            self.signals.mod.osc.copy_from(mod_osc.signals, osc_signames + ["sync_input", "sync_input_phase"])
            self.signals.mod.env = SignalContainer()
            self.signals.mod.env.copy_from(mod_env.signals, env_signames)
            # we can also rename signals
            self.signals.mod.env.depth = mod_env.signals.gain
            self.signals.mod.osc.shift = mp.signals.shift[0]

This patch exposes many signals with reasonable effort. You might ask, why not just expose the internal
plugins directly and give users full freedom? This makes sense at first but has two drawbacks:

- We should be able to rename and regroup signals. For example, the ``multipitch`` plugin has signals
  that are both represented in ``.signals`` as well as ``.signals.mod.osc``, in one case under a different
  name, in the other as a single signal instead of a tuple. This is generally very useful to make nice
  user interfaces.

- Exposing too much plugin detail makes your patch inflexible for changes in the future. Say you discover
  an alternative configuration that sounds better and also uses 30% less CPU (it has happened to us), then
  downstream uses might break when you change the internals. In our example, we wanted to expose a lot
  of detail and have essentially locked ourselves in on the ``osc`` plugin, but we are not particular
  about ``multipitch`` and could easily replace it with a few ``buffer`` and ``range_shifter`` plugins.

Let's play with it!

.. code-block:: python

    synth = test_melody(blm.new(ModSynth))
    for x in range(12):
        synth.signals.mod.osc.shift.tone = x
        print(synth.signals.mod.osc.shift)
        time.sleep_ms(1200)

That is not as disharmonic as you'd expect from modulating with unrelated waveforms; this is because we
have added a tiny yet powerful secret: The ``.sync_*`` signals of ``osc`` allow us to reset one oscillator
when another completes a cycle. We've used this in :ref:`Patterns<examples-patterns-hard-sync>` before to
create rhythms, but of course in the audio domain it works too. Our primary oscillator resets the modulator
each cycle, forcing the modulator into a periodic waveform! We've exposed this connection; let's disconnect
and see what happens:

.. code-block:: python

    synth.signals.mod.osc.sync_input << None
    for x in range(12):
        synth.signals.mod.osc.shift.tone = x
        print(synth.signals.mod.osc.shift)
        time.sleep_ms(1200)

Yeah, that's the good disharmonic overtones. Note how there is a nontrivial connection between pitch offset
and disharmonicity. As a rule of thumb, simple fractions often make for harmonic intervals, but they may imply
a different pitch than either oscillator is playing.

Lastly, remember that pitch dependent phase offset we mentioned earlier? If we restore the hard sync, we
can play around with that too, for example by modulating it with an LFO:

.. code-block:: python

    synth.signals.mod.osc.sync_input << synth.signals.osc.sync_output
    lfo = blm.new(bl00mbox.plugins.osc)
    lfo.signals.freq = 1
    lfo.signals.output >> synth.signals.mod.osc.sync_input_phase
    for x in range(12):
        synth.signals.mod.osc.shift.tone = x
        print(synth.signals.mod.osc.shift)
        time.sleep_ms(1200)

Additive synthesis
^^^^^^^^^^^^^^^^^^

Messing around randomly with oscillators feeding back into each other is neat, but sometimes we want to do
something more controlled. Let's check out additive synthesis!

One of the most basic models of tonal sounds is that they consist of (sine) harmonics whose frequencies
are integer multiples of the perceived frequency of the perceived tonal sound. The sound can "evolve"
in time by changing the volume levels of its overtone components.

Pure additive synthesizers use a sinewave generator for each individual harmonic and are therefore
inefficient for signals with high amounts of overtones, but we can go less pure and use non-sinusoidal
(or even nonperiodic) waveforms to group overtones together. 

.. code-block:: python

    class AdditiveSynth(bl00mbox.Patch):
        def __init__(self, chan, harmonics = (1, 3, 5), atten_dB = -6):
            super().__init__(chan)
            decays = [2000,500,200]
            attacks = [100,50,10]
            #
            # create basic interfaces
            mixer = self.new(bl00mbox.plugins.mixer, len(harmonics))
            multipitch = self.new(bl00mbox.plugins.multipitch, len(harmonics))
            self.signals.pitch = multipitch.signals.input 
            self.signals.output = mixer.signals.output
            self.signals.trigger = multipitch.signals.trigger_in
            #
            harmonics_sigs = []
            tot_gain = 0
            for x in range(len(harmonics)):
                env = self.new(bl00mbox.plugins.env_adsr)
                osc = self.new(bl00mbox.plugins.osc)
                mixer.signals.input[x] << env.signals.output
                env.signals.input << osc.signals.output
                env.signals.trigger << multipitch.signals.trigger_thru
                multipitch.signals.output[x] >> osc.signals.pitch
                # attenuate higher harmonics more 
                env.signals.gain.dB = atten_dB * x
                tot_gain += env.signals.gain.mult
                # set up different envelopes for each overtone
                if x < 3:
                    env.signals.attack.value = attacks[x]
                    env.signals.decay.value = decays[x]
                env.signals.sustain.value = 0
                multipitch.signals.shift[x].freq *= harmonics[x]
                sigs = SignalContainer()
                sigs.copy_from(env.signals, ["release", "attack", "decay", "sustain", "gain"])
                sigs.shift = multipitch.signals.shift[x]
                harmonics_sigs.append(sigs)
            # normalize volume
            mixer.signals.gain.mult = 0.5
            for x in range(len(harmonics)):
                env.signals.gain.mult /= tot_gain
            self.signals.harmonics = SignalTuple(harmonics_sigs)
                

    additive_synth = test_melody(blm.new(AdditiveSynth))

Note how we used the ``.freq`` setter to go to the integer multiple. Since this is a pitch shift
type signal, it feels a bit counterintuitive, but remember that it is encoded just like pitch signal
so that ``.tone = 0`` is ``.freq = 440``. We're using the initialized default value here to multiply
off of to hide this imperfection

While it's a good starting point to think of harmonics as integer multiples of a root frequency, 
in most physical tonal instruments like (non-bowed) strings and woodwinds they are actually
`out of tune <https://en.wikipedia.org/wiki/Inharmonicity>`_. Let's try a different init:

.. code-block:: python

    harmonics_semitone = (0, 7 + 12, 4 + 24)
    for x, harmonic in enumerate(harmonics_semitone):
        additive_synth.signals.harmonics[x].shift.tone = harmonic

This time we used semitone quantization which results in "out-of-tune" harmonics, but our ear is very
used to them as modern music likes to tune its instruments like this. The difference is not much, but
there's a slightly different undulation to it if you listen closely. If you have large chords that sound
out of tune, maybe check with this one if it's not overtone clashes. Because of our ``.freq`` hack above
we can undo it in this slightly more complicated way:

.. code-block:: python

    harmonics_multiple = (1, 3, 5)
    for harmonic in enumerate(harmonics_multiple):
        additive_synth.signals.harmonics[x].shift.freq = 440 * harmonic

This oscillator gets its character not only from its harmonics tuning, but also from its envelope settings.
If you play around with them you will find a range of usable sounds!

Wavetables
^^^^^^^^^^

Here's 2 hacks to do different forms of wavetable synthesis. Note how they can be modulated differently.
The distortion-based one does currently not have enough resolution for low frequency applications, but
we will provide a way for that soon.

.. code-block:: python

    wavetable_py = [random.randint(-32767, 32767) for x in 129]
    wavetable_file = "waveform_110Hz.wav"

    class WaveShaper(bl00mbox.Patch):
        def __init__(self, chan, wavetable):
            super().__init__(self, chan)

            osc = self.new(bl00mbox.plugins.osc)
            dist = self.new(bl00mbox.plugins.distortion)
            
            # Time in a wavetable oscillator runs like a sawtooth.
            osc.signals.waveform.switch.SAW = True
            dist.signals.input << osc.signals.output

            # Simply put the table into the distortion lookup table
            dist.curve = wavetable

            # This turns off any smoothing when the sawtooth jumps back
            # to "roll over" to the start instead of "scrolling back"
            osc.antialiasing = True

            self.signals.pitch = osc.signals.pitch
            self.signals.output = dist.signals.output

    class WavePlayer(bl00mbox.Patch):
        def __init__(self, chan, file_path, file_pitch):
            super().__init__(self, chan)

            # load the wavetable from a short wavefile at a path
            # pitch of wavefile sample should be near low end of
            # desired range to limit artifacts.
            sampler = self.new(bl00mbox.plugins.sampler, path)

            self.signals.pitch = sampler.signals.replay_speed
            self.signals.output = sampler.signals.output

            # we're using the replay speed signal to modulate pitch.
            # since this is a pitch-type signal, it should play
            # a pitch of 440Hz by default. Say our file contains
            # a 110Hz waveform then we must pretend it's at 4x sample
            # rate to compensate.
            sampler.sample_rate *= 440/file_pitch
        
    wavetable_synth_type_one = blm.new(WaveShaper, wavetable_py)
    wavetable_synth_type_two = blm.new(WavePlayer, wavetable_file, 110)


Paraphony
^^^^^^^^^

Our ``ModSynth`` patch is pretty cool, but if you wanna run a bunch of them into a shared ``mixer`` you're
wasting CPU as the individual filters per voice are redundant. We can build patches with subassemblies that
can be taken apart as needed by a caller that is familiar with the convention. Let's a create a minimal patch:

.. code-block:: python

    class ParaphonicSynth(bl00mbox.Patch):
        class ParaphonicCommon(bl00mbox.Patch):
            def __init__(self, blm):
                super().__init__(blm)
                filt = self.new(bl00mbox.plugins.filter)
                filt.signals.cutoff.freq = 3000
                filt.signals.gain.dB = -6
                self.signals.input = filt.signals.input
                self.signals.output = filt.signals.output
                self.signals.filter = SignalContainer()
                self.signals.filter.copy_from(filt.signals, ["cutoff", "reso", "mix"])
        #
        class ParaphonicVoice(bl00mbox.Patch):
            def __init__(self, blm):
                super().__init__(blm)
                osc = self.new(bl00mbox.plugins.osc)
                env = self.new(bl00mbox.plugins.env_adsr)
                osc.signals.output >> env.signals.input
                self.signals.output = env.signals.output
                self.signals.trigger = env.signals.trigger
                self.signals.pitch = osc.signals.pitch
                self.signals.osc = SignalContainer()
                self.signals.osc.copy_from(osc.signals, ["waveform", "morph", "fm"])
                self.signals.env = SignalContainer()
                self.signals.env.copy_from(env.signals, ["attack", "release", "decay", "sustain"])
                self.signals.gain = env.signals.gain
                self.signals.fx_loop = SignalContainer()
                self.signals.fx_loop.send = osc.signals.output
                self.signals.fx_loop.ret = env.signals.input
        #
        def __init__(self, blm):
            super().__init__(blm)
            voice = self.new(self.ParaphonicVoice)
            common = self.new(self.ParaphonicCommon)
            voice.signals.fx_loop.send >> common.signals.input
            voice.signals.fx_loop.ret << common.signals.output
            self.signals = SignalContainer()
            self.signals.copy_from(voice.signals, ["output", "pitch", "trigger", "osc", "env", "gain"])
            self.signals.filter = common.signals.filter

We can then create a patch that accepts a patch like this as an initializer argument. Sometimes it's
convenient to accept input for a lot of voices (like 10 notes for flow3r's 10 petals) but run into CPU
issues. For that case you can use a ``poly_squeeze`` plugin to limit the number of active voices. This
is also often good for sound clarity and headroom.

.. _examples-synth-multi-synth:

.. code-block:: python

    class PolySynth(bl00mbox.Patch):
        def __init__(self, blm, patch, num_voices, num_inputs):
            super().__init__(blm)
            if num_voices > num_inputs:
                num_voices = num_inputs
            mixer = self.new(bl00mbox.plugins.mixer, num_voices)
            self.paraphonic = hasattr(patch, "ParaphonicCommon") \
                          and hasattr(patch, "ParaphonicVoice")
            if self.paraphonic:
                self.common = self.new(patch.ParaphonicCommon)
                self.common.signals.input << mixer.signals.output
                self.signals.output = self.common.signals.output
                voice_patch = patch.ParaphonicVoice
            else:
                self.signals.output = self.mixer.signals.output
                voice_patch = patch
            # squeeze branch: don't expose voice triggers/pitch signals
            # directly but shove a poly_squeeze plugin in between
            squeeze = num_voices != num_inputs
            if squeeze:
                poly = self.new(bl00mbox.plugins.poly_squeeze, num_voices, num_inputs)
            else:
                pitch_sigs = []
                trigger_sigs = []
            voices = []
            for x in range(num_voices):
                voice = self.new(voice_patch)
                voice.signals.output >> mixer.signals.input[x]
                voices.append(voice)
                if squeeze:
                    voice.signals.pitch << poly.signals.pitch_out[x]
                    voice.signals.trigger << poly.signals.trigger_out[x]
                else:
                    pitch_sigs.append(voice.signals.pitch)
                    trigger_sigs.append(voice.signals.trigger)
            self.voices = tuple(voices)
            if squeeze:
                self.signals.pitch = poly.signals.pitch_in
                self.signals.trigger = poly.signals.trigger_in
            else:
                self.signals.pitch = SignalTuple(pitch_sigs)
                self.signals.trigger = SignalTuple(trigger_sigs)
            self.signals.gain = mixer.signals.gain

We can try it like this:

.. code-block:: python

    num_inputs = 12
    poly = blm.new(PolySynth, ParaphonicSynth, 5, num_inputs)
    poly.signals.output >> blm.signals.line_out

    for x in range(num_inputs * 2):
        if x < num_inputs:
            poly.signals.pitch[x].tone = -15 + x * 3.5
            poly.signals.trigger[x].start()
        else:
            index = 2 * num_inputs - 1 - x
            poly.signals.trigger[index].stop()
        time.sleep_ms(300)

We can hear that when new notes are added so that we don't have enough oscillators for all of them,
the sound of the oldest notes stop automatically to make space. They are not gone forever though: If
we stop newer notes, those "suspended" notes that are still in "started" state are brought back.

For the application above this behavior could of course also be scripted in micropython, but this
approach allows us to automate things in the background:

.. code-block:: python

    for x in range(num_inputs):
        osc = blm.new(bl00mbox.plugins.osc)
        osc.signals.pitch.freq = 0.5 * (1 + (x % 0.565))
        osc.signals.sync_output >> poly.signals.trigger[x]

There is a subtlety in terms of CPU use here: If many oscillators are running, it saves us a considerable
amount of filter time, however if none are running the paraphonic version is actually more expensive:
Since the filter is now behind the individual envelopes it is never shut down by them, it runs at all times
when the ``.output`` signal is rendered. In comparision, the ``.fx_loop`` insert path of ``ParaphonicVoice``
had allowed us to place it in front of the envelope so that it doesn't get rendered if that note isn't playing.

It depends on the individual use case which option is preferable. We do intend to add features that would allow
us to gate the global effects easily as well.

This ``.fx_loop`` also has the advantage that patch users can easily add more effects to the path that also
benefit from this optimization. It would of course work poorly with reverbs and thelike that can produce
audio even if their input is muted, but many others such as flangers or bitcrushers will work just fine.
            

Effects
-------

.. _examples-effects-tremolo:

Tremolo
^^^^^^^

Let's get started with one of the most basic effects: Tremolo is a mainstay in many forms of music and simply
describes periodic variation in volume. We can get volume control out of the ``mixer`` plugin, so we can easily
hook up an ``osc`` for modulation and some ``range_shifter`` plugins for interfacing:

.. code-block:: python

    class Tremolo(bl00mbox.Patch):
        def __init__(self, chan):
            super().__init__(chan)
            mixer = self.new(bl00mbox.plugins.mixer, 1)
            osc = self.new(bl00mbox.plugins.osc)
            osc_gain = self.new(bl00mbox.plugins.range_shifter)
            depth = self.new(bl00mbox.plugins.range_shifter)
            depth.speed = "slow"
            #
            self.signals.input = mixer.signals.input[0]
            self.signals.output = mixer.signals.output
            self.signals.speed = osc.signals.pitch
            self.signals.depth = depth.signals.input
            self.signals.waveform = osc.signals.waveform
            self.signals.morph = osc.signals.morph
            #
            osc.signals.pitch.freq = 5
            self.signals.depth = 32767
            #
            depth.signals.input_range[0].value = 0
            depth.signals.output_range[0].mult = 1
            depth.signals.output_range[1].mult = 0
            depth.signals.output >> osc_gain.signals.output_range[0]
            osc_gain.signals.output_range[1].mult = 1
            #
            osc.signals.output >> osc_gain.signals.input
            osc_gain.signals.output >> mixer.signals.input_gain[0]
            self._loglike = False

        @property
        def loglike(self):
            return self._loglike

        @loglike.setter
        def loglike(self, val):
            val = bool(val)
            if val == self._loglike:
                return
            if val:
                mixer.signals.gain << osc_gain.signals.output
            else:
                mixer.signals.gain << None
            self._loglike = val

The ``loglike`` property uses a simple trick: Natural volume perception is not linear, but logarithmic, so many
consider good tremolos to implement a log-like behavior. To achieve this, we simply apply gain twice, therefore
applying it with a square-law which is an okay approximation. Note how setting it to ``None`` restores the
original static value rather than holding the last one given by ``osc_gain.signals.output``.

Manual reverb
^^^^^^^^^^^^^

bl00mbox does not have reverb plugin yet, but it has delay. Use a lot of delay to get away with everything.
We are not particularily great at the whole allpass coefficients business, so our reverbs always come out
a bit lofi, but here's one that you can use in a pinch:

.. code-block:: python

    class ManualReverb(bl00mbox.Patch):
        def __init__(self, chan):
            super().__init__(chan)
            num_delays = 4
            delays = [self.new(bl00mbox.plugins.delay_static) for x in range(num_delays)]
            mixers = [self.new(bl00mbox.plugins.mixer, num_delays) for x in range(num_delays)]
            buffer = self.new(bl00mbox.plugins.buffer)
            #
            self.signals.input = buffer.signals.input
            self.signals.output = mixers[0].signals.output
            self.signals.volume = delays[0].signals.level
            #
            for x in range(1, num_delays):
                delays[x].signals.input << mixers[x].signals.output
                delays[x].signals.output >> mixers[0].signals.input[x]
                mixers[x].signals.input[0] << delays[0].signals.output
                for j in range(1, num_delays):
                    delays[j].signals.output >> mixers[x].signals.input[j]
                delays[x].signals.feedback = 0
                delays[x].signals.dry_vol = 0
                delays[x].signals.time = min(30 * (x ** (3 / 2)), 500)
                mixers[x].signals.gain.value = -4096 * 0.4
            #
            buffer.signals.output >> delays[0].signals.input
            mixers[0].signals.input[0] << buffer.signals.output
            mixers[0].signals.gain.mult = 1
            delays[0].signals.time.value = 100
            delays[0].signals.level.value = 0
            delays[0].signals.feedback.value = 0
            delays[0].signals.dry_vol.value = 0

Note that modulating delay time sounds very crunchy at this point in time, but if you can add vibrato
to the source you can obtain chorus-like effects without it.

Noise
-----

Here's some examples for sound generators that can't be associated with a traditional pitch.
They are useful for percussive sounds (when used with an envelope generator) or textures.
They also can be used as modulation sources for tonal synthesizers to add a bit more "dirt"
to the sound.

Feedback
^^^^^^^^

Some nonlinearity and some filtering together with a kickstart can often result in interesting waveforms.
If it doesn't work, flip the phase, try more gain, or add more filters :D!

.. warning::

    Filters are currently somewhat unstable, see their documentation on the ``bl00mbox.plugins`` page.
    Since we are heavily overdriving filters here, these topologies are unstable as well and might
    start sounding significantly differently or stop working at all in future iterations. Make sure
    to test and if necessary adjust them if you update to a future bl00mbox version.

.. code-block:: python

    class FeedbackSynth(bl00mbox.Patch):
        def __init__(self, chan): 
            super().__init__(chan) 
            f = self.new(bl00mbox.plugins.filter)
            m = self.new(bl00mbox.plugins.mixer, 2)
            f.signals.output >> m.signals.input[0]
            f.signals.input << m.signals.output
            #
            f.signals.mode.switch.BANDPASS = True
            f.signals.cutoff.tone = -1
            f.signals.reso.value = -12000
            f.signals.mix.value = -18000
            #
            # some gain and DC filtering
            f.signals.gain.dB = 18
            m.signals.input_gain[0].dB = 18
            m.signals.gain.dB = 18
            m.block_dc = True
            #
            self.signals.output = f.signals.output
            # pitch is not exact here due to one-buffer delay
            # of circular dependencies
            self.signals.pitch = f.signals.cutoff
            #
            # kickstart
            m.signals.input[1] = 32767


Let's make some noise!

.. code-block:: python

    noise = blm.new(FeedbackSynth)
    # noise!
    noise.signals.output >> blm.signals.line_out
    # different noise!
    noise.signals.pitch.tone = 6
    # more different noise!
    noise.signals.pitch.tone = -12
    # ...tone?
    noise.signals.pitch.tone = -24

We have found an interesting quality of this type of synthesizer: They sometimes collapse into a stable
oscillation state where they just play a tone. *Note: Where exactly this occurs might depend on the exact
bl00mbox version and config you're running. This example was created on flow3r.*

Let's experiment some more:

.. code-block:: python

    # return to previous setting: it was noise before, now it's tone?
    noise.signals.pitch.tone = 6
    # going back to the start, we have noise as expected
    noise.signals.pitch.tone = 0
    # re-return: this time it's noise again?
    noise.signals.pitch.tone = 6

It appears that at a setting of ``.tone = 6`` the synthesizer is metastable: Depending on its previous state,
it may collapse either into stable oscillation or noise.

We can see that these feedback loops have very complex, organic and interesting behavior. This has been a strong
suit of the analog domain so far: For genres like harsh noise, people have long been feeding back mixer channels
into themselves (often via guitar pedals) for decades. The example above is a good starting point, but you can
build much more complex networks out of virtually any plugin that bl00mbox provides, so the sky is the limit!

.. warning::

    These loops exploit many implementation details of plugins, so any small change can have unpredictable
    effects on them. As bl00mbox is still young and plugins are immature, we are in a bad position to provide
    any sort of stability: We need to upgrade features and fix bugs after all. This means that version upgrades
    (as happen frequently for flow3r) may cause changes in behavior, requiring maintenance. It may be a good
    idea to keep reference recordings at hand.

If this sounds disheartening, consider the importance of impermance in art: Not everything must be
reproducible. Equally, future updates will make your creation evolve chaotically and organically, is this
process not valuable by itself?
