Backend
=======

.. warning::

    We've actually never tried this way, we just hooked up the backend to flow3r audio codec
    once and then never touched it again. We have no idea how the build tree of micropython
    right now looks like or what's a good place in init to spawn the audio render task and
    all that, so you'll maybe need a while to get this running.

Render API
----------

bl00mbox only requires you to use two functions in ``bl00mbox.h`` to get started, but they
aren't great: They take misleading arguments and are not very flexible beyond flow3r's use
case. Apologies for the state of things, we've been swamped, we will fix them up someday.
Let's take a quick look before we go into their issues:

.. c:function:: void bl00mbox_init(void)

    This one is easy, it should be simply be called exactly once before the micropython module
    is imported or ``bl00mbox_audio_render()`` is called.

.. c:function:: void bl00mbox_audio_render(int16_t * rx, int16_t * tx, uint16_t len)

    At first glance this looks very intuitive: A buffer ``rx`` with data from the line in jack etc.,
    a buffer ``tx`` where bl00mbox will write rendered audio to, and an int ``len`` to tell us how
    long these buffers are. This isn't true at all though.

So what's wrong with the render function?

Buffer size
^^^^^^^^^^^

Here's the catch: The buffer length must always be 64. The big motivation for this is
circular dependencies: Their efficient implementation relies on always-constant buffer size
since they simply take a leftover buffer from the last run. This seems like a fairly intense
constraint, but consider:

- Circular dependencies are commonplace in modular synths for many use cases such as highly
  networked LFO swagger generators. For audio their application field suffers a bit from the
  buffer latency, but they are very useful for interesting noise-like textures.

- If we want to mellow out CPU load with a larger buffer, we can still do so and use a
  a series of ``bl00mbox_audio_render()`` calls over moving-64-window buffers. This does not
  result in the same overhead reduction, but bl00mbox plugins are generally not super high in
  overhead.

- bl00mbox is intended to be a low-latency, responsive system. Buffer length directly introduces
  latency, so we don't want to run much larger buffers than our 1.4ms long ones. Smaller
  buffers don't typically make much sense either.

- Constant buffer signals are much more predictable if all buffers have the same shape. We could
  of course also define a second sample rate at 1/64th of the primary one and keep count but that's
  less efficient, and the entire point of constant buffers is to run fast.

We could allow for an arbitrary constant buffer size, but this would reduce portability of
patches that use feedback in the audio domain; we've decided to go all in instead and just
proclaim the fixed size of 64. And then we "forgot" to update the API.

Buffer format
^^^^^^^^^^^^^

bl00mbox itself is strictly mono at this point in time. However, ``bl00mbox_audio_render()``
expects stereo-interleaved buffers so that they both are twice the length in ``int16_t`` as
indicated. This should just be part of the function name, and we could benefit from supporting
other audio formats as well, but it is what it is for now.

Sample rate
^^^^^^^^^^^

Did you notice how we never set the sample rate? bl00mbox runs at a sample rate of 48kHz. Always.
We will add more options in the future, but for now all timing and pitch is derived from this, so
you're gonna have an interesting time running on another :P.

Tasks and Locks
---------------

Setup
^^^^^

Find a suitable place in the micropython init code to add some extra steps. The most accessible
one might be before the micropython task is started, just search for ``xTaskCreate`` (for FreeRTOS
ports). Add the following steps before micropython runs:

1) Call ``bl00mbox_init()``
2) Initialize audio codec and set up for 48kHz sample rate
3) Create audio codec task (see below)
4) Proceed to start the micropython task as before

Audio codec task
^^^^^^^^^^^^^^^^

In the simplest setup we can just render audio in the same task that handles I2S communication
with the codec. You'd typically want a task with higher priority than micropython that goes through
something like these steps:

1) prepare 64 stereo interlaced samples in ``tx`` via ``bl00mbox_audio_render()``
2) yield task until codec is ready to receive data
3) transmit ``tx`` to codec
4) receive 64 stereo interlaced samples from codec into ``rx``
5) go back to 1)

``rx`` should be initialized with zeroes for the first ``bl00mbox_audio_render()`` call. If
your codec does not feature audio input, you may skip step 4).

It is vital that the rendering task may run as unhindered as possible as it needs to compute
new buffers with a hard deadline. If you experience issues, you can use FreeRTOS profiling features
to see what tasks might choke you.

Locks
^^^^^

bl00mbox by default uses FreeRTOS locks: micropython bl00mbox APIs will occassionally take a lock
briefly to add an element to a linked list or so. Rendering may not resume until that lock is
returned. If you use FreeRTOS you don't need to worry about this causing issues since it provides
locks with priority inheritance. We're certain that Zephyr can provide this as well.

However, if you were to operate outside of FreeRTOS and simply replace them with ``atomic`` locks
or the like, a task that is below priority of the audio task but above the micropython task may
prevent micropython from returning the lock, therefore blocking rendering and causing audio glitches.

If you intend to run bl00mbox well without an operating system that provides a suitable locking
mechanism, this will probably be your biggest challenge. It certainly can be done though ;D!

Multicore rendering
^^^^^^^^^^^^^^^^^^^

Why not run bl00mbox across both cores? Channes provide convenient compartmentalization. As usual,
we simply haven't gotten around to it yet, but it's totally doable; feel free to dig around in
``bl00mbox_audio.c`` if you want to try things out. The biggest challengea are figuring out how to
handle ``rx`` buffers when a deadline has been missed, and some mechanism to prevent it from flooding
both cores completely. Micropython should always get *some* compute.

External Audio API
------------------

There are software features that bl00mbox does **not** provide but that you'd want in an audio project
anyways. It might help with scope estimates and such.

Hardware
^^^^^^^^

There's still some beautifully primitive codecs out there, just set up the I2S bus and you're running.
But then you have to bring your own headphone amp, and that's so convenient to have in the same package,
so you get a fancy one and hook it up to an I2C bus and write a nice default config!

Next step, implement the two dozen or so most important APIs. This is boring and you will find yourself
trying new text editor hacks to efficiently transfer the table for line in gain from datasheet to source
code.

If you want to avoid a few good hours of that, do find a codec that somebody else has already exposed
to micropython, else do allocate some time for creating these interfaces.

Other audio sources
^^^^^^^^^^^^^^^^^^^

Due to its simple API bl00mbox can easily be embedded in a system of multiple audio sources. For example,
you can route the output of an mp3 decoder into ``rx`` and then apply bl00mbox effects to it. Granted,
it would be mixed down to mono, but that's okay. You might wanna think about what kind of signal flow
options are interesting to you and how expose them as API.

At this point in time bl00mbox does not help you with this, but at some point external sources at least
might be able to use the channel infrastructure for mixing and RMS and all maybe probably.
