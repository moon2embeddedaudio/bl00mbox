Frontend
========

Linking files
-------------

Hooking up the audio side in C was the hardest part. We can almost get going, we just need to move some
files in the right location. The ``project`` path is a placeholder of course.

- ``bl00mbox/micropython/mp_sys_bl00mbox.c`` should be symlinked into the ``project/micropython/usermodule/``
  directory. The build system can usually be informed of this new file by registering it in
  ``project/micropython/usermodule/micropython.cmake``.
  If this worked you should now be able to ``import sys_bl00mbox``.

- You still need the actual bl00mbox module. bl00mbox provides this in the ``bl00mbox/micropython/bl00mbox``
  directory. You can either move this into micropython's ``frozenmodules`` (recommended for most users) or
  store it in editable plaintext. Symlinks are often your friend here again. If this worked you
  should now be able to ``import bl00mbox``.

Your bl00mbox installation should now be almost fully functional!

Setting up callbacks
--------------------

Callbacks can be arbitrary micropython objects, and your port needs to decide how to process them. You can
use `flow3r style callbacks <https://docs.flow3r.garden/app/guide/bl00mbox.html#run-in-background>`_ as a
simple template.

Your micropython environment can now be set up to periodically retrieve callbacks of all active channels and
process them like here:

.. warning::

    It should go without saying, but flow3r apps should never access ``bl00mbox.Sys`` features.

.. code-block:: python

    import bl00mbox

    while True:
        run_main_loop(delta_ms)
        for callback in bl00mbox.Sys.collect_active_callbacks():
            callback(delta_ms)

You may of course also choose to only run one callback after each main loop execution. We provide no means
of calling these automatically, so if your application does not have a main loop you might need to get
creative yourself if you wish to use this feature.

Using the global mixer
----------------------

Channels may take a second form with the ``SysChannel(Channel)`` class, which implements a few extra features for
the global mixer on top of a regular ``Channel`` object:

.. warning::

    It should go without saying, but flow3r apps should never access ``bl00mbox.Sys`` features.

.. code-block:: python

    # get only the active channels
    sys_chans = bl00mbox.Sys.collect_channels(active = True)
    for sys_chan in sys_chans:
        # let's randomize volumes a bit!

        # like .gain_dB, but invisible to the Channel object
        sys_chan.sys_gain_dB += random.random(-6, 6)
        # like .rms_dB, but includes system volume
        print(sys_chan.sys_rms_dB)

Implementing a UI for this mixer is up to you.

