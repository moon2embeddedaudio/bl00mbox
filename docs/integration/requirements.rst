Requirements
============

Microcontroller
---------------

bl00mbox is primarily developed on the esp32s3, but bun should run well
on many other modern microcontollers. Here's what we'd look for:

- Basics:
    - 32bit ALU
    - I2S port
    - >150MHz clock

- Micropython port with real time operating system:
    - FreeRTOS: All is done
    - Zephyr: You have to port binary semaphores with priority inheritance 

- Nice to have:
    - Seperate core for UI
    - Few hundred kB of RAM or more
    - Fast int32_t division
    - Fast MULSH, i.e. ``int32_t a,b,c; c = ((int64_t) a * b) >> 32``

Audio codec
-----------

- Run at 48kHz sample rate. bl00mbox only runs at 48kHz right now.
  It's weird, we know.
