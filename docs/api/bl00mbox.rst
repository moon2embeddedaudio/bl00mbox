``bl00mbox`` module
===================

.. py:class:: Channel

    .. py:method:: __init__(name: str = "repl") -> None:

        Initializes ``Channel`` object with given name. Always name your channels.

    .. py:attribute:: name
        :type: string

        (r/w) Name of the channel.

    .. py:method:: new(thing: type[Plugin | Patch], \*args) -> Plugin | Patch:

        This method is used to create plugins or patches (groups of plugins) within the
        channel:

        .. code-block:: python

            import bl00mbox
            blm = bl00mbox.Channel("example")
            osc = blm.new(bl00mbox.plugins.osc)

        Some plugins take initialization parameters, which may be passed after the
        plugin or patch type:

        .. code-block:: python

            # create mixer plugin with 8 input channels
            mixer = blm.new(bl00mbox.plugins.mixer, 8)

        These parameters are documented in the sections of the individual plugins.

        \*args are forwarded to the plugin/patch constructor. *Ignore the extra backslash above,
        it's a vim syntax highlighting thing ;)*

    .. py:attribute:: signals
        :type: SignalList

        The signals associated with the channel. May vary depending on implementation,
        but at least should include the ``.line_out`` signal. Connect to this signal
        to send the sound your plugins create to your device output. Often (for example
        in the case of flow3r) there is also a ``.line_in`` signal to receive audio from
        external systems.

    .. py:attribute:: foreground
        :type: bool

        (r/w) Whether this channel is currently in the foreground or not. Foreground
        channels are always rendered if their effective volume is nonzero.

    .. py:attribute:: background_mute_override
        :type: bool

        (r/w) Whether this channel will be rendered even if it is not in the foreground
        unless its effective volume is nonzero.

    .. py:attribute:: gain_dB
        :type: float

        (r/w) Gain of the channel in dB, default value -12dB, maximum 0dB. Use ``-math.inf``
        to mute the channel. This suspends rendering.

    .. py:attribute:: compute_rms
        :type: bool

        (r/w) Whether this channel should continously calculate its rms volume. Consumes
        a bit of CPU, ideally only enable when actually using the ``.rms_dB`` output. Note
        that the response time of ``.rms_dB`` is not instantaneous, i.e. reading it right
        after enabling ``.compute_rms`` will result in very low readouts.

    .. py:attribute:: rms_dB
        :type: float

        (r) If ``.compute_rms`` is ``True``, returns root-mean-squared volume of channel
        in decibels (0dB represents a full-scale sine wave). Will return ``-math.inf`` if
        there is no signal or ``.compute_rms`` is ``False``.

    .. py:method:: clear() -> None:

        Deletes all plugins in the channel. Attempting to interact with a deleted
        plugin will result in a ``bl00mbox.ReferenceError``.

    .. py:method:: delete() -> None:

        Deletes the channel and all plugins in it. Attempting to interact with a
        deleted channel or plugin will result in a ``bl00mbox.ReferenceError``.

    .. py:attribute:: callback

        (r/w) Callback function. Bl00mbox provides an API for the surrounding operating
        system to return this function if the channel is currently being rendered,
        effectively allowing to execute micropython for background channels. The type
        of this callback is defined by the operating system (in case of flow3r: see
        flow3r-specific bl00mbox documentation in the flow3r docs).

.. py:class:: Plugin

    .. py:attribute:: signals
        :type: SignalList

        The signals associated with the plugin. Check documentation of individual plugins
        for details.

    .. py:attribute:: always_render
        :type: bool

        (r/w) A plugin is only rendered when another plugin requests data from it. This
        requires at least a signal connection, but that alone may not suffice: The ``env_adsr``
        plugin for example will not request data when the envelope is at 0 to conserve CPU.

        Setting this flag makes sure the plugin is rendered even if no data is requested
        from it.

    .. py:method:: delete() -> None:

        Deletes the plugin. Attempting to interact with a deleted plugin will result in a
        ``bl00mbox.ReferenceError``. Note: Plugins will also be deleted when their channel
        is garbage collected.

.. py:class:: Signal

    Signals come in input and output flavors. Input signals receive data, output signals provide.
    data. Look up in the documentation or use the ``.__repr__()`` method to find out which they are.

    .. py:method:: __repr__() -> str:

        Helper function that returns a string describing the signal and its current state.

    .. py:method:: __rshift__() -> none:

    .. py:method:: __lshift__() -> None:

        The right and left shift operators are used to connect signals. They must always
        point from an output-type signal to an input-type signal, else a ``TypeError`` is raised:

        .. code-block:: python

            import bl00mbox
            blm = bl00mbox.Channel("example")
            osc = blm.new(bl00mbox.plugins.osc)
            mixer = blm.new(bl00mbox.plugins.mixer, 8)

            osc.signals.output >> mixer.signals.input[0]
            blm.signals.line_out << mixer.signals.output

            # this one will fail since both are inputs:
            osc.signals.morph << mixer.signals.input_gain[0]

        One output signal can be connected to multiple input signals, but each input signal may
        only be connected to one output. To disconnect a signal , you can use the same operation
        with ``None`` or set it to a fixed value:

        .. code-block:: python

            # disconnects mixer.signals.output and
            # connects osc.signals.output
            blm.signals.line_out << osc.signals.output

            # disconnects both blm.signals.line_out
            # and mixer.signals.input[0]
            osc.signals.output >> None
            

    .. py:attribute:: value
        :type: int

        (r/(w for input signals)) The current value of the signal. Range from -32767..32767 (note that ``INT16_MIN``
        is not included). This is in many cases an abstract number, so there exist a set of context-dependent
        representation members as listed below. These follow the same (r/w) rules.

        See ``bl00mbox.helpers`` section for more conversion details.

    .. py:attribute:: tone
        :type: float

        Representation for pitch-type signals: Semitone distance from A440.

    .. py:attribute:: freq
        :type: float

        Representation for pitch-type signals: Frequency in Hertz.

    .. py:attribute:: mult
        :type: float

        Representation for gain-type signals: Gain as multiplier, specifically ``.value/4096``.

    .. py:attribute:: dB
        :type: float

        Representation for gain-type signals: Gain in dB. Can be ``-math.inf``. 0dB represents a ``.value`` of 4096.
        The sign of ``.value`` is ignored.

.. py:class:: Patch

    .. py:attribute:: signals
        :type: SignalList

        The signals associated with the patch. Check documentation of individual patches for details.

    .. py:method:: delete() -> None:

        Deletes all plugins within the patch. Attempting to interact with a deleted plugin will result
        in a ``bl00mbox.ReferenceError``. Note: Plugins will also be deleted when their channel is
        garbage collected.

    .. py:method:: new(thing: type[Plugin | Patch], \*args) -> Plugin | Patch:

        Creates new plugin/patch that is tied to this patch instance and deleted when its ``.delete()`` is called.

        \*args are forwarded to the plugin/patch constructor.

        Unlike with ``Channel.new()``, this method should not be called by the patch user but only be used by the patch
        internally to set itself up. Maybe we'll underscore it in the future.
