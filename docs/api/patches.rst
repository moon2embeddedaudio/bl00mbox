``bl00mbox.patches`` module
===========================

.. warning::

    If you check out the actual ``_patches.py`` file, you will see a bunch of patches not listed here. These
    are from the very early days of flow3r and very much deprecated, do not use them in new applications.

Oof, it's barren here. One stuck around from the early days of flow3r, but this feature has been somewhat
underdeveloped, there were always more pressing things. You may be able to find more interesting patches to
copy-paste into your projects by checking the ``Techniques`` page or by searching for ``bl00mbox.Patch`` /
``bl00mbox.patches._Patch`` in the flow3r firmware (be aware that many there follow an antiquated coding style
tho). Check the ``Patches`` page if you want to modify them or make your own.

.. py:class:: tinysynth(bl00mbox.Patch)

    Simple synthesizer consisting of an oscillator and an envelope generator.

    .. py:attribute:: signals.output
        :type: SignalOutput

        Output of the synthesizer.

    .. py:attribute:: signals.pitch
        :type: SignalInput

        Pitch-type input to set pitch of the synthesizer.

    .. py:attribute:: signals.trigger
        :type: SignalInput

        Trigger-type input to play the synthesizer. Accepts ``start``, ``stop`` and honours ``velocity``.

    .. py:attribute:: signals.attack
        :type: SignalInput

        Compare to ``bl00mbox.plugins.env_adsr`` parameter of same name.

    .. py:attribute:: signals.decay
        :type: SignalInput

        Compare to ``bl00mbox.plugins.env_adsr`` parameter of same name.
        Default 500.

    .. py:attribute:: signals.sustain
        :type: SignalInput

        Compare to ``bl00mbox.plugins.env_adsr`` parameter of same name.

    .. py:attribute:: signals.release
        :type: SignalInput

        Compare to ``bl00mbox.plugins.env_adsr`` parameter of same name.
        Default 100.
