``bl00mbox.helpers`` module
===========================

This module provides mostly unit conversions for now. It is quite barren at this point, we hope we can
make it prettier soon. For flow3r 1.4 purposes, you may check the copypasta expansions below.

Unit conversion
---------------

Bl00mbox pitch-type units internally are called ``sct`` (semi-centi-tone, or 2400 units per octave). You'd typically
get these from the ``.value`` attribute of a pitch-type signal. This is a logarithmic signal type and indicates
distance from A440, which is encoded as ``18367``, so that the resulting frequency is ``(2**((sct-18367)/2400)) * 440Hz``.
This is a bit weird, but by using helper functions you usually don't have to deal with it. It spans a frequency range
from ``0.01bpm`` to ``28kHz``, so it works well for both pitch and rhythm.

.. py:function:: sct_to_freq(sct: int) -> float

    Returns the frequency corresponding to the ``sct`` value as per the definition above.

.. py:function:: sct_to_note_name(sct: int) -> str

    Name of nearest note to ``sct`` value with scientific octave notation (i.e. 440Hz is ``"A4"``).
    Always uses flats instead of sharps, e.g. (``"Gb2"``).

.. py:function:: note_name_to_sct(name: str) -> int

    Inverse of sct_to_note_name. Accepts a single sharp or flat.

Gain-type signals are simple multipliers with unity gain at ``4096``. We have no helpers for them in the module
yet, but there are examples below if you need the functionality.

Copypasta Expansions
--------------------

We will add this functionality in a clean form soon, but for now you can copy-paste these into
your project as needed.

.. code-block:: python

    import math

    SCT_A440 = 18367
    SCT_OCTAVE = 2400
    GAIN_UNITY = 4096

    def freq_to_sct(freq: float):
        '''
        Inverse of sct_to_freq
        '''
        return math.log(freq/440, 2) * SCT_OCTAVE + SCT_A440

    def sct_to_tone(sct: int):
        '''
        Returns how many semitones away from A440 the ``sct`` value is.
        '''
        return 12 * (sct - SCT_A440) / SCT_OCTAVE

    def tone_to_sct(tone: float):
        '''
        inverse of sct_to_tone
        '''
        return (tone * SCT_OCTAVE) // 12 + SCT_A440

    def sct_to_bpm(sct: int):
        '''
        Returns how many beats per minute the ``sct`` value corresponds to.
        '''
        freq = sct_to_freq(sct)
        return freq * 60

    def bpm_to_sct(bpm: float):
        '''
        Inverse of sct_to_bpm
        '''
        return freq_to_sct(bpm/60)

    def gain_to_mult(gain: int):
        '''
        Interprets ``.value`` of gain-type signal as a multiplier.
        '''
        return gain / GAIN_UNITY

    def mult_to_gain(mult: float):
        '''
        Inverse of gain_to_mult
        '''
        return int(mult * GAIN_UNITY)
        
    def gain_to_dB(gain: int):
        '''
        Interprets ``.value`` of gain-type signal as decibels. Drops sign.
        '''
        if not gain:
            return -math.inf
        else if gain < 0:
            gain = -gain
        return 20 * math.log(gain / GAIN_UNITY, 10)

    def dB_to_gain(decibels: float):
        '''
        Inverse of gain_to_dB. Does not restore sign.
        '''
        if decibels == -math.inf:
            return 0
        return (10 ** (decibels / 20)) * GAIN_UNITY
