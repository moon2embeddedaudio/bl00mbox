import os
import sys
import shutil
import subprocess
import time

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'bl00mbox'
copyright = '2025'
author = 'moon2'

# The full version, including alpha/beta/rc tags
release = (
    subprocess.check_output(["git", "describe", "--tags", "--long", "--always"]).decode().strip()
)
release += "\n"
release += time.strftime("%F %R")
version = release

release = ""
version = ""

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
    'myst_parser',
    'sphinx_multiversion',
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

pygments_style = 'sphinx'

maximum_signature_line_length = 120


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_logo = 'bl00mbox_logo.png'
html_css_files = [
    'css/custom.css',
]
html_theme_options = {
    'logo_only': True,
    'style_nav_header_background': "#000",
    'style_external_links': True,
}

# Show "Edit on GitLab" links
html_show_sourcelink = False
html_context = {
    'display_gitlab': False,
    'gitlab_host': "gitlab.com",
    'gitlab_user': "moon2embeddedaudio",
    'gitlab_repo': "bl00mbox",
    'gitlab_version': "main/",
    'conf_py_path': "docs/",
    'theme_vcs_pageview_mode': "edit",
}

smv_branch_whitelist = r'v\d+\.\d+\.\d+$|^main$|^flow3r'
smv_remote_whitelist = r'^origin$'

def setup(app):
    pass
