Troubleshooting
===============

Sound
-----

Oscillator stops playing sound after changing pitch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Did you use the right setter for adjusting pitch? For a pitch-type signal, ``.tone = 0`` is a very audible 440Hz,
whereas ``.value = 0`` is a subsonic 2.2Hz growl.

Popping/clicking when making connections
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You are probably switching DC in the signal path. The ``Channel.signals.line_out`` signals have their own built-in
DC blocking, and if you need to add one somewhere in the middle of the signal path the ``mixer`` plugin can optionally
switch on a DC block. Note that these typically need to sample a bit of your signal first to determine DC levels, so
it is recommended to combine this with a fade-in.

Single channel sounds distorted
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

bl00mbox uses 16-bit buffers. This is generally okay for audio, but requires us setting gain between stages manually.
For example, the ``osc`` plugin provides full-range output, so going directly into a resonant ``filter`` plugin may
easily result in distortion since the resonance may amplify a high-energy portion of the spectrum. The ``filter``
plugin has its own ``gain`` signal to avoid that, but if there's no built-in solution you can always use a single
channel ``mixer`` plugin as a volume control.

DC on the signal may also lead to early distortions since one of the halfwaves experiences reduced headroom. Once
again the ``mixer`` plugin can help with its DC blocking feature.

All channels crackle
^^^^^^^^^^^^^^^^^^^^

This is likely a CPU overload where bl00mbox cannot provide samples in time. Ideally, your operating system
provides some form to check that (flow3r provides serial printout via the ftop setting). There are a few
words on performance optimization in `flow3r-specific docs <https://docs.flow3r.garden/app/guide/bl00mbox.html#performance>`_
that actually are very general and will be moved over here at some point.

API
---

Everything in flow3r looks different
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We have recently overhauled our API, so many things shifted. We want to make an effort to keep flow3r stock applications
up-to-date, but we didn't the capacity to do so before the deadline for the flow3r v1.4 firmware. Here's the most important
changes:

We used to override ``.__setattr__()`` of ``SignalList`` so that we could connect signals with the assigment syntax
``osc.signals.output = mixer.signals.input[0]``. This is generally a terrible idea since the functionality is not
provided by the signal, but by its container, meaning if you juggle signals around elsewhere you cannot use it anymore:

.. code-block:: python

    # this makes connection
    osc.signals.output = mixer.signals.input[0]
    # let's store a reference elsewhere
    cool_signal = osc.signals.output
    # this does not make a connection, instead
    # it just overrides cool_signal to a reference
    # with the mixer input :/
    cool_signal = mixer.signals.input[0]

Equally, you could set the ``.value`` with this neat shorthand that also fails silently and is hard to debug, so we're
not supporting it anymore:

.. code-block:: python

    # old style, don't use
    osc.signals.waveform = 4000
    # new style
    osc.signals.waveform.value = 4000

Patches used to be a bit more hacky in the past, so you may have seen some patterns like this:

.. code-block:: python

    # old style: inherit from hidden reference
    class OldPatch(bl00mbox.patches._Patch):
        def __init__(self, chan):
            super().__init__(self)
            # old style #1: spawn from the channel object passed as an arument
            self.plugins.osc = chan.new(bl00mbox.plugins.osc)
            # old style #2: spawn from hidden reference
            self.plugins.noise = self._channel.new(bl00mbox.plugins.noise)
            # we manually store in self.plugins, it is necessary for .delete()

This seems like minor differences, but there has been a conceptual shift in how patches work: We now define all
nonexposed surfaces as unstable. With the old approach all plugins were forced to be exposed in the ``.plugins``
container in order for ``.delete()`` to work, which would freeze much of the inner workings of a patch. This
proved to be a major nuisance when trying to evolve individual patches, so we decided to switch to an approach
where only the "plugin-like" outer surface of a patch must remain stable. 

``ReferenceError``
^^^^^^^^^^^^^^^^^^

bl00mbox implements this error type for channels, plugins and signals. For channels it's easy: If you called
the ``.delete()`` method, interaction attempts may throw that error henceforth as the channel is no more.

For plugins it is more subtle: While the same behavior goes for their ``.delete()`` methods, they also are
deleted when their corresponding channel is deleted.

Garbage collection on both channels and plugins effectively calls the respective ``.delete()`` methods too.
This means that if you don't keep a reference of the channel object, all its plugins will be deleted the
next time garbage collection runs.

.. code-block:: python

    class ChannelDropper:
        def forgor(self):
            blm = bl00mbox.Channel("forgor")
            self.noise = blm.new(bl00mbox.plugins.noise)
            blm.signals.line_out << self.noise.signals.output
            # when this method exits the channel becomes unreachable and
            # will be garbage collected, so if you keep doing unrelated
            # things in micropython, the sound will eventually drop out.

        def rember(self):
            blm = bl00mbox.Channel("rember")
            self.noise = blm.new(bl00mbox.plugins.noise)
            blm.signals.line_out << self.noise.signals.output
            # this time let's store it somewhere :D
            # this will prevent the sound from dropping out
            self.blm = blm

        def random_value(self):
            if hasattr(self, "noise"):
                # this may fail with ReferenceError if forgor was used
                return self.noise.signals.output.value
        

Note that many flow3r applications do not follow the flow3r-specific channel deletion guidelines as explained in
the `flow3r-specific bl00mbox documentation <https://docs.flow3r.garden/app/guide/bl00mbox.html#make-sound>`_.
This may lead to crashes and other issues now or in the future, so please take care to not just copy and paste
from another flow3r application without checking if it follows good practices.
