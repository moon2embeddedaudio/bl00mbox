���U      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Troubleshooting�h]�h	�Text����Troubleshooting�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�X/tmp/tmpqdlq4i72/fe811d0a529267931cde2b50165441cc6df68eaa/docs/guide/troubleshooting.rst�hKubh)��}�(hhh]�(h)��}�(h�Sound�h]�h�Sound�����}�(hh0hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh-hhhh,hKubh)��}�(hhh]�(h)��}�(h�3Oscillator stops playing sound after changing pitch�h]�h�3Oscillator stops playing sound after changing pitch�����}�(hhAhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh>hhhh,hKubh	�	paragraph���)��}�(h��Did you use the right setter for adjusting pitch? For a pitch-type signal, ``.tone = 0`` is a very audible 440Hz,
whereas ``.value = 0`` is a subsonic 2.2Hz growl.�h]�(h�KDid you use the right setter for adjusting pitch? For a pitch-type signal, �����}�(hhQhhhNhNubh	�literal���)��}�(h�``.tone = 0``�h]�h�	.tone = 0�����}�(hh[hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhhQubh�" is a very audible 440Hz,
whereas �����}�(hhQhhhNhNubhZ)��}�(h�``.value = 0``�h]�h�
.value = 0�����}�(hhmhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhhQubh� is a subsonic 2.2Hz growl.�����}�(hhQhhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hK
hh>hhubeh}�(h!]��3oscillator-stops-playing-sound-after-changing-pitch�ah#]�h%]��3oscillator stops playing sound after changing pitch�ah']�h)]�uh+h
hh-hhhh,hKubh)��}�(hhh]�(h)��}�(h�(Popping/clicking when making connections�h]�h�(Popping/clicking when making connections�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubhP)��}�(hX�  You are probably switching DC in the signal path. The ``Channel.signals.line_out`` signals have their own built-in
DC blocking, and if you need to add one somewhere in the middle of the signal path the ``mixer`` plugin can optionally
switch on a DC block. Note that these typically need to sample a bit of your signal first to determine DC levels, so
it is recommended to combine this with a fade-in.�h]�(h�6You are probably switching DC in the signal path. The �����}�(hh�hhhNhNubhZ)��}�(h�``Channel.signals.line_out``�h]�h�Channel.signals.line_out�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubh�x signals have their own built-in
DC blocking, and if you need to add one somewhere in the middle of the signal path the �����}�(hh�hhhNhNubhZ)��}�(h�	``mixer``�h]�h�mixer�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubh�� plugin can optionally
switch on a DC block. Note that these typically need to sample a bit of your signal first to determine DC levels, so
it is recommended to combine this with a fade-in.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKhh�hhubeh}�(h!]��(popping-clicking-when-making-connections�ah#]�h%]��(popping/clicking when making connections�ah']�h)]�uh+h
hh-hhhh,hKubh)��}�(hhh]�(h)��}�(h�Single channel sounds distorted�h]�h�Single channel sounds distorted�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubhP)��}�(hX�  bl00mbox uses 16-bit buffers. This is generally okay for audio, but requires us setting gain between stages manually.
For example, the ``osc`` plugin provides full-range output, so going directly into a resonant ``filter`` plugin may
easily result in distortion since the resonance may amplify a high-energy portion of the spectrum. The ``filter``
plugin has its own ``gain`` signal to avoid that, but if there's no built-in solution you can always use a single
channel ``mixer`` plugin as a volume control.�h]�(h��bl00mbox uses 16-bit buffers. This is generally okay for audio, but requires us setting gain between stages manually.
For example, the �����}�(hh�hhhNhNubhZ)��}�(h�``osc``�h]�h�osc�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubh�F plugin provides full-range output, so going directly into a resonant �����}�(hh�hhhNhNubhZ)��}�(h�
``filter``�h]�h�filter�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubh�s plugin may
easily result in distortion since the resonance may amplify a high-energy portion of the spectrum. The �����}�(hh�hhhNhNubhZ)��}�(h�
``filter``�h]�h�filter�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubh�
plugin has its own �����}�(hh�hhhNhNubhZ)��}�(h�``gain``�h]�h�gain�����}�(hj'  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubh�a signal to avoid that, but if there’s no built-in solution you can always use a single
channel �����}�(hh�hhhNhNubhZ)��}�(h�	``mixer``�h]�h�mixer�����}�(hj9  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhh�ubh� plugin as a volume control.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKhh�hhubhP)��}�(h��DC on the signal may also lead to early distortions since one of the halfwaves experiences reduced headroom. Once
again the ``mixer`` plugin can help with its DC blocking feature.�h]�(h�|DC on the signal may also lead to early distortions since one of the halfwaves experiences reduced headroom. Once
again the �����}�(hjQ  hhhNhNubhZ)��}�(h�	``mixer``�h]�h�mixer�����}�(hjY  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhjQ  ubh�. plugin can help with its DC blocking feature.�����}�(hjQ  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKhh�hhubeh}�(h!]��single-channel-sounds-distorted�ah#]�h%]��single channel sounds distorted�ah']�h)]�uh+h
hh-hhhh,hKubh)��}�(hhh]�(h)��}�(h�All channels crackle�h]�h�All channels crackle�����}�(hj|  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjy  hhhh,hK"ubhP)��}�(hX�  This is likely a CPU overload where bl00mbox cannot provide samples in time. Ideally, your operating system
provides some form to check that (flow3r provides serial printout via the ftop setting). There are a few
words on performance optimization in `flow3r-specific docs <https://docs.flow3r.garden/app/guide/bl00mbox.html#performance>`_
that actually are very general and will be moved over here at some point.�h]�(h��This is likely a CPU overload where bl00mbox cannot provide samples in time. Ideally, your operating system
provides some form to check that (flow3r provides serial printout via the ftop setting). There are a few
words on performance optimization in �����}�(hj�  hhhNhNubh	�	reference���)��}�(h�X`flow3r-specific docs <https://docs.flow3r.garden/app/guide/bl00mbox.html#performance>`_�h]�h�flow3r-specific docs�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name��flow3r-specific docs��refuri��>https://docs.flow3r.garden/app/guide/bl00mbox.html#performance�uh+j�  hj�  ubh	�target���)��}�(h�A <https://docs.flow3r.garden/app/guide/bl00mbox.html#performance>�h]�h}�(h!]��flow3r-specific-docs�ah#]�h%]��flow3r-specific docs�ah']�h)]��refuri�j�  uh+j�  �
referenced�Khj�  ubh�J
that actually are very general and will be moved over here at some point.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hK$hjy  hhubeh}�(h!]��all-channels-crackle�ah#]�h%]��all channels crackle�ah']�h)]�uh+h
hh-hhhh,hK"ubeh}�(h!]��sound�ah#]�h%]��sound�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�API�h]�h�API�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK*ubh)��}�(hhh]�(h)��}�(h�$Everything in flow3r looks different�h]�h�$Everything in flow3r looks different�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK-ubhP)��}�(h��We have recently overhauled our API, so many things shifted. We want to make an effort to keep flow3r stock applications
up-to-date, but we didn't the capacity to do so before the deadline for the flow3r v1.4 firmware. Here's the most important
changes:�h]�hX  We have recently overhauled our API, so many things shifted. We want to make an effort to keep flow3r stock applications
up-to-date, but we didn’t the capacity to do so before the deadline for the flow3r v1.4 firmware. Here’s the most important
changes:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hK/hj�  hhubhP)��}�(hX]  We used to override ``.__setattr__()`` of ``SignalList`` so that we could connect signals with the assigment syntax
``osc.signals.output = mixer.signals.input[0]``. This is generally a terrible idea since the functionality is not
provided by the signal, but by its container, meaning if you juggle signals around elsewhere you cannot use it anymore:�h]�(h�We used to override �����}�(hj   hhhNhNubhZ)��}�(h�``.__setattr__()``�h]�h�.__setattr__()�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj   ubh� of �����}�(hj   hhhNhNubhZ)��}�(h�``SignalList``�h]�h�
SignalList�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj   ubh�< so that we could connect signals with the assigment syntax
�����}�(hj   hhhNhNubhZ)��}�(h�/``osc.signals.output = mixer.signals.input[0]``�h]�h�+osc.signals.output = mixer.signals.input[0]�����}�(hj,  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj   ubh��. This is generally a terrible idea since the functionality is not
provided by the signal, but by its container, meaning if you juggle signals around elsewhere you cannot use it anymore:�����}�(hj   hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hK3hj�  hhubh	�literal_block���)��}�(hX!  # this makes connection
osc.signals.output = mixer.signals.input[0]
# let's store a reference elsewhere
cool_signal = osc.signals.output
# this does not make a connection, instead
# it just overrides cool_signal to a reference
# with the mixer input :/
cool_signal = mixer.signals.input[0]�h]�hX!  # this makes connection
osc.signals.output = mixer.signals.input[0]
# let's store a reference elsewhere
cool_signal = osc.signals.output
# this does not make a connection, instead
# it just overrides cool_signal to a reference
# with the mixer input :/
cool_signal = mixer.signals.input[0]�����}�hjF  sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��force���language��python��highlight_args�}�uh+jD  hh,hK7hj�  hhubhP)��}�(h��Equally, you could set the ``.value`` with this neat shorthand that also fails silently and is hard to debug, so we're
not supporting it anymore:�h]�(h�Equally, you could set the �����}�(hj[  hhhNhNubhZ)��}�(h�
``.value``�h]�h�.value�����}�(hjc  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj[  ubh�n with this neat shorthand that also fails silently and is hard to debug, so we’re
not supporting it anymore:�����}�(hj[  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKBhj�  hhubjE  )��}�(h�`# old style, don't use
osc.signals.waveform = 4000
# new style
osc.signals.waveform.value = 4000�h]�h�`# old style, don't use
osc.signals.waveform = 4000
# new style
osc.signals.waveform.value = 4000�����}�hj{  sbah}�(h!]�h#]�h%]�h']�h)]�jT  jU  jV  �jW  �python�jY  }�uh+jD  hh,hKEhj�  hhubhP)��}�(h�^Patches used to be a bit more hacky in the past, so you may have seen some patterns like this:�h]�h�^Patches used to be a bit more hacky in the past, so you may have seen some patterns like this:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKLhj�  hhubjE  )��}�(hX�  # old style: inherit from hidden reference
class OldPatch(bl00mbox.patches._Patch):
    def __init__(self, chan):
        super().__init__(self)
        # old style #1: spawn from the channel object passed as an arument
        self.plugins.osc = chan.new(bl00mbox.plugins.osc)
        # old style #2: spawn from hidden reference
        self.plugins.noise = self._channel.new(bl00mbox.plugins.noise)
        # we manually store in self.plugins, it is necessary for .delete()�h]�hX�  # old style: inherit from hidden reference
class OldPatch(bl00mbox.patches._Patch):
    def __init__(self, chan):
        super().__init__(self)
        # old style #1: spawn from the channel object passed as an arument
        self.plugins.osc = chan.new(bl00mbox.plugins.osc)
        # old style #2: spawn from hidden reference
        self.plugins.noise = self._channel.new(bl00mbox.plugins.noise)
        # we manually store in self.plugins, it is necessary for .delete()�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�jT  jU  jV  �jW  �python�jY  }�uh+jD  hh,hKNhj�  hhubhP)��}�(hX  This seems like minor differences, but there has been a conceptual shift in how patches work: We now define all
nonexposed surfaces as unstable. With the old approach all plugins were forced to be exposed in the ``.plugins``
container in order for ``.delete()`` to work, which would freeze much of the inner workings of a patch. This
proved to be a major nuisance when trying to evolve individual patches, so we decided to switch to an approach
where only the "plugin-like" outer surface of a patch must remain stable.�h]�(h��This seems like minor differences, but there has been a conceptual shift in how patches work: We now define all
nonexposed surfaces as unstable. With the old approach all plugins were forced to be exposed in the �����}�(hj�  hhhNhNubhZ)��}�(h�``.plugins``�h]�h�.plugins�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj�  ubh�
container in order for �����}�(hj�  hhhNhNubhZ)��}�(h�``.delete()``�h]�h�	.delete()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj�  ubhX   to work, which would freeze much of the inner workings of a patch. This
proved to be a major nuisance when trying to evolve individual patches, so we decided to switch to an approach
where only the “plugin-like” outer surface of a patch must remain stable.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKZhj�  hhubeh}�(h!]��$everything-in-flow3r-looks-different�ah#]�h%]��$everything in flow3r looks different�ah']�h)]�uh+h
hj�  hhhh,hK-ubh)��}�(hhh]�(h)��}�(h�``ReferenceError``�h]�hZ)��}�(hj�  h]�h�ReferenceError�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKaubhP)��}�(h��bl00mbox implements this error type for channels, plugins and signals. For channels it's easy: If you called
the ``.delete()`` method, interaction attempts may throw that error henceforth as the channel is no more.�h]�(h�sbl00mbox implements this error type for channels, plugins and signals. For channels it’s easy: If you called
the �����}�(hj�  hhhNhNubhZ)��}�(h�``.delete()``�h]�h�	.delete()�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj�  ubh�X method, interaction attempts may throw that error henceforth as the channel is no more.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKchj�  hhubhP)��}�(h��For plugins it is more subtle: While the same behavior goes for their ``.delete()`` methods, they also are
deleted when their corresponding channel is deleted.�h]�(h�FFor plugins it is more subtle: While the same behavior goes for their �����}�(hj  hhhNhNubhZ)��}�(h�``.delete()``�h]�h�	.delete()�����}�(hj%  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj  ubh�L methods, they also are
deleted when their corresponding channel is deleted.�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKfhj�  hhubhP)��}�(h��Garbage collection on both channels and plugins effectively calls the respective ``.delete()`` methods too.
This means that if you don't keep a reference of the channel object, all its plugins will be deleted the
next time garbage collection runs.�h]�(h�QGarbage collection on both channels and plugins effectively calls the respective �����}�(hj=  hhhNhNubhZ)��}�(h�``.delete()``�h]�h�	.delete()�����}�(hjE  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hYhj=  ubh�� methods too.
This means that if you don’t keep a reference of the channel object, all its plugins will be deleted the
next time garbage collection runs.�����}�(hj=  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hKihj�  hhubjE  )��}�(hXu  class ChannelDropper:
    def forgor(self):
        blm = bl00mbox.Channel("forgor")
        self.noise = blm.new(bl00mbox.plugins.noise)
        blm.signals.line_out << self.noise.signals.output
        # when this method exits the channel becomes unreachable and
        # will be garbage collected, so if you keep doing unrelated
        # things in micropython, the sound will eventually drop out.

    def rember(self):
        blm = bl00mbox.Channel("rember")
        self.noise = blm.new(bl00mbox.plugins.noise)
        blm.signals.line_out << self.noise.signals.output
        # this time let's store it somewhere :D
        # this will prevent the sound from dropping out
        self.blm = blm

    def random_value(self):
        if hasattr(self, "noise"):
            # this may fail with ReferenceError if forgor was used
            return self.noise.signals.output.value�h]�hXu  class ChannelDropper:
    def forgor(self):
        blm = bl00mbox.Channel("forgor")
        self.noise = blm.new(bl00mbox.plugins.noise)
        blm.signals.line_out << self.noise.signals.output
        # when this method exits the channel becomes unreachable and
        # will be garbage collected, so if you keep doing unrelated
        # things in micropython, the sound will eventually drop out.

    def rember(self):
        blm = bl00mbox.Channel("rember")
        self.noise = blm.new(bl00mbox.plugins.noise)
        blm.signals.line_out << self.noise.signals.output
        # this time let's store it somewhere :D
        # this will prevent the sound from dropping out
        self.blm = blm

    def random_value(self):
        if hasattr(self, "noise"):
            # this may fail with ReferenceError if forgor was used
            return self.noise.signals.output.value�����}�hj]  sbah}�(h!]�h#]�h%]�h']�h)]�jT  jU  jV  �jW  �python�jY  }�uh+jD  hh,hKmhj�  hhubhP)��}�(hX�  Note that many flow3r applications do not follow the flow3r-specific channel deletion guidelines as explained in
the `flow3r-specific bl00mbox documentation <https://docs.flow3r.garden/app/guide/bl00mbox.html#make-sound>`_.
This may lead to crashes and other issues now or in the future, so please take care to not just copy and paste
from another flow3r application without checking if it follows good practices.�h]�(h�uNote that many flow3r applications do not follow the flow3r-specific channel deletion guidelines as explained in
the �����}�(hjm  hhhNhNubj�  )��}�(h�i`flow3r-specific bl00mbox documentation <https://docs.flow3r.garden/app/guide/bl00mbox.html#make-sound>`_�h]�h�&flow3r-specific bl00mbox documentation�����}�(hju  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name��&flow3r-specific bl00mbox documentation�j�  �=https://docs.flow3r.garden/app/guide/bl00mbox.html#make-sound�uh+j�  hjm  ubj�  )��}�(h�@ <https://docs.flow3r.garden/app/guide/bl00mbox.html#make-sound>�h]�h}�(h!]��&flow3r-specific-bl00mbox-documentation�ah#]�h%]��&flow3r-specific bl00mbox documentation�ah']�h)]��refuri�j�  uh+j�  j�  Khjm  ubh��.
This may lead to crashes and other issues now or in the future, so please take care to not just copy and paste
from another flow3r application without checking if it follows good practices.�����}�(hjm  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hOhh,hK�hj�  hhubeh}�(h!]��referenceerror�ah#]�h%]��referenceerror�ah']�h)]�uh+h
hj�  hhhh,hKaubeh}�(h!]��api�ah#]�h%]��api�ah']�h)]�uh+h
hhhhhh,hK*ubeh}�(h!]��troubleshooting�ah#]�h%]��troubleshooting�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�root_prefix��/��source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks���sectnum_xform���strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform���sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j�  j�  h�h�h�h�jv  js  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  �h��hՉjv  �j�  �j�  �j�  �j�  �j�  �j�  �uh!}�(j�  hj�  h-h�h>h�h�js  h�j�  jy  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.