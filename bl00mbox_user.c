//SPDX-License-Identifier: CC0-1.0
#include "bl00mbox_user.h"

extern void bl00mbox_disconnect_rx_callback(void * rx, uint16_t signal_index);
extern void bl00mbox_connect_rx_callback(void * rx, void * tx, uint16_t signal_index);
extern void bl00mbox_disconnect_mx_callback(void * chan, void * tx);
extern void bl00mbox_connect_mx_callback(void * chan, void * tx);

// get signal struct from a signal index
radspa_signal_t * bl00mbox_signal_get_by_index(radspa_t * plugin, uint16_t signal_index){
    return &(plugin->signals[signal_index]);
}

static inline bl00mbox_connection_t * conn_from_signal(bl00mbox_signal_t * signal){
    return (bl00mbox_connection_t *) signal->rignal->buffer;
}

static bool signal_is_input(bl00mbox_signal_t * signal){
    return signal->rignal->hints & RADSPA_SIGNAL_HINT_INPUT ? true : false;
}

static bool signal_is_output(bl00mbox_signal_t * signal){
    return signal->rignal->hints & RADSPA_SIGNAL_HINT_OUTPUT ? true : false;
}

static bool signal_equals(bl00mbox_signal_t * some, bl00mbox_signal_t * other){
    return (some->plugin == other->plugin) && (some->index == other->index);
}

static bool signals_are_connected(bl00mbox_signal_t * some, bl00mbox_signal_t * other){
    if(!some->rignal->buffer) return false;
    return some->rignal->buffer == other->rignal->buffer;
}

bool bl00mbox_signal_is_output(bl00mbox_signal_t * signal){
    return signal_is_output(signal);
}

bool bl00mbox_signal_is_input(bl00mbox_signal_t * signal){
    return signal_is_input(signal);
}

bl00mbox_connection_t * bl00mbox_connection_from_signal(bl00mbox_signal_t * signal){
    return conn_from_signal(signal);
}

uint16_t bl00mbox_channel_plugins_num(bl00mbox_channel_t * chan){
    return chan->plugins.len;
}

bl00mbox_array_t * bl00mbox_channel_collect_plugins(bl00mbox_channel_t * chan){
    unsigned int len = chan->plugins.len;
    bl00mbox_array_t * ret;
    ret = malloc(sizeof(bl00mbox_array_t) + len * sizeof(void *));
    if(!ret) return NULL;
    ret->len = len;

    bl00mbox_set_iter_t iter;
    bl00mbox_set_iter_start(&iter, &chan->plugins);
    bl00mbox_plugin_t * plugin;
    size_t i = 0;
    while((plugin = bl00mbox_set_iter_next(&iter))) ret->elems[i++] = plugin;
    return ret;
}

uint16_t bl00mbox_channel_conns_num(bl00mbox_channel_t * chan){
    return chan->connections.len;
}

uint16_t bl00mbox_channel_mixer_num(bl00mbox_channel_t * chan){
    return chan->roots.len;
}

bl00mbox_array_t * bl00mbox_channel_collect_connections_mx(bl00mbox_channel_t * chan){
    bl00mbox_array_t * ret;
    ret = malloc(sizeof(bl00mbox_array_t) + chan->roots.len * sizeof(void *));
    if(!ret) return NULL;

    ret->len = chan->roots.len;

    bl00mbox_set_iter_t iter;
    bl00mbox_set_iter_start(&iter, &chan->roots);
    bl00mbox_connection_t * conn;
    size_t i = 0;
    while((conn = bl00mbox_set_iter_next(&iter))){
        ret->elems[i++] = &conn->source;
    }
    return ret;
}

bl00mbox_array_t * bl00mbox_signal_collect_connections(bl00mbox_signal_t * signal){
    // caller has to free memory
    // return NULL -> OOM error
    bl00mbox_array_t * ret = NULL;
    bl00mbox_connection_t * conn = conn_from_signal(signal);
    if(!conn){
        ret = malloc(sizeof(bl00mbox_array_t));
        if(!ret) return NULL;
        ret->len = 0;
    } else if(signal->rignal->hints & RADSPA_SIGNAL_HINT_INPUT){
        ret = malloc(sizeof(bl00mbox_array_t) + sizeof(void *));
        if(!ret) return NULL;
        ret->len = 1;
        ret->elems[0] = &conn->source;
    } else {
        // TODO: add sentinel for channel mixer connection
        ret = malloc(sizeof(bl00mbox_array_t) + conn->subscribers.len * sizeof(void *));
        if(!ret) return NULL;
        ret->len = conn->subscribers.len;

        bl00mbox_set_iter_t iter;
        bl00mbox_set_iter_start(&iter, &conn->subscribers);
        bl00mbox_signal_t * sub;
        size_t i = 0;
        while((sub = bl00mbox_set_iter_next(&iter))){
            ret->elems[i++] = sub;
        }
    }
    return ret;
}

static void update_roots(bl00mbox_channel_t * chan){
    // create list of plugins to be rendered, i.e. all roots and always_actives
    bl00mbox_set_t * roots = &chan->roots; // content: bl00mbox_connection_t
    bl00mbox_set_t * always_render = &chan->always_render; // content: bl00mbox_plugin_t
    radspa_signal_t * output_rignal = &chan->channel_plugin->rugin->signals[1];
    
    // render_buffers are simple, just copy the content of the set and add the output plugin manually
    int num_render_buffers = roots->len + (output_rignal->buffer ? 1 : 0);
    bl00mbox_array_t * render_buffers = malloc(sizeof(bl00mbox_array_t) + sizeof(void *) * num_render_buffers);
    // for render_plugins there may be duplicates. we still allocate full memory for now
    int num_render_plugins = roots->len + always_render->len + (output_rignal->buffer ? 1 : 0);
    bl00mbox_array_t * render_plugins = malloc(sizeof(bl00mbox_array_t) + sizeof(void *) * num_render_plugins);
    if(!(render_buffers && render_plugins)) goto defer;

    size_t index = 0;

    bl00mbox_set_iter_t iter;
    // filling up mixer roots
    bl00mbox_set_iter_start(&iter, roots);
    bl00mbox_connection_t * conn;
    while((conn = bl00mbox_set_iter_next(&iter))){
        render_buffers->elems[index] = conn->buffer;
        render_plugins->elems[index] = conn->source.plugin;
        index++;
    }
    render_buffers->len = index;

    // if someone is connected to the channel_plugin output:
    // add to render_buffers/render_plugin
    if(output_rignal->buffer){
        render_buffers->elems[index] = output_rignal->buffer;
        render_buffers->len++;

        conn = output_rignal->buffer;
        bl00mbox_plugin_t * plugin = conn->source.plugin;
        bool is_duplicate = false;
        for(size_t rindex = 0; rindex < index; rindex++){
            if(render_plugins->elems[rindex] == plugin){
                is_duplicate = true;
                break;
            }
        }
        if(!is_duplicate){
            render_plugins->elems[index++] = conn->source.plugin;
        }
    }

    // adding always_render to the plugin list. those should be after the regular roots
    // because we might mess with natural render order otherwise
    size_t duplicate_index = index;
    bl00mbox_set_iter_start(&iter, always_render);
    bl00mbox_plugin_t * plugin;
    while((plugin = bl00mbox_set_iter_next(&iter))){
        // check for duplicates
        bool is_duplicate = false;
        for(size_t rindex = 0; rindex < duplicate_index; rindex++){
            if(render_plugins->elems[rindex] == plugin){
                is_duplicate = true;
                break;
            }
        }
        if(!is_duplicate) render_plugins->elems[index++] = plugin;
    }
    render_plugins->len = index;

    // if we overshot let's trim excess memory.
    if(index != num_render_plugins){
        bl00mbox_array_t * tmp = realloc(render_plugins, sizeof(bl00mbox_array_t) + sizeof(void *) * index);
        if(tmp) render_plugins = tmp;
    }

defer:
    if(!(render_plugins && render_buffers)){
        free(render_plugins);
        free(render_buffers);
        render_plugins = NULL;
        render_buffers = NULL;
        bl00mbox_log_error("out of memory, render list cleared")
    }
#ifdef BL00MBOX_DEBUG
    else {
        bl00mbox_log_info("new render data, %d plugins, %d buffers",
        (int) render_plugins->len, (int) render_buffers->len);
    }
#endif
    bl00mbox_array_t * render_plugins_prev = chan->render_plugins;
    bl00mbox_array_t * render_buffers_prev = chan->render_buffers;
    bl00mbox_take_lock(&chan->render_lock);
    chan->render_plugins = render_plugins;
    chan->render_buffers = render_buffers;
    bl00mbox_give_lock(&chan->render_lock);
    free(render_plugins_prev);
    free(render_buffers_prev);
}

bl00mbox_plugin_t * bl00mbox_plugin_create_unlisted(bl00mbox_channel_t * chan, radspa_descriptor_t * desc, uint32_t init_var){
    // doesn't instantiate, don't free
    bl00mbox_plugin_t * plugin = calloc(1, sizeof(bl00mbox_plugin_t));
    if(plugin == NULL) return NULL;
    radspa_t * rugin = desc->create_plugin_instance(init_var);
    if(rugin == NULL){ free(plugin); return NULL; }

    plugin->init_var = init_var;
    plugin->rugin = rugin;
    plugin->channel = chan;
    plugin->id = chan->plugin_id++;
    return plugin;
}

bl00mbox_plugin_t * bl00mbox_plugin_create(bl00mbox_channel_t * chan, uint32_t id, uint32_t init_var){
    // doesn't instantiate, don't free
    radspa_descriptor_t * desc = bl00mbox_plugin_registry_get_descriptor_from_id(id);
    if(desc == NULL) return NULL;

    bl00mbox_plugin_t * plugin = bl00mbox_plugin_create_unlisted(chan, desc, init_var);

    if(!bl00mbox_set_add_skip_unique_check(&chan->plugins, plugin)){
        plugin->rugin->descriptor->destroy_plugin_instance(plugin->rugin);
        free(plugin);
        return NULL;
    }

    bl00mbox_channel_event(chan);
    return plugin;
}

void bl00mbox_plugin_destroy(bl00mbox_plugin_t * plugin){
    bl00mbox_channel_t * chan = plugin->channel;

    // remove from parent
    if(* (plugin->parent_self_ref) != plugin){
        bl00mbox_log_error("plugin: parent_self_ref improper: plugin %s, %p, ref %p",
            plugin->rugin->descriptor->name, plugin, * (plugin->parent_self_ref));
    }
    * (plugin->parent_self_ref) = NULL;

    // disconnect all signals
    int num_signals = plugin->rugin->len_signals;
    for(int i = 0; i < num_signals; i++){
        bl00mbox_signal_t sig = {
            .index = i,
            .plugin = plugin,
            .rignal = &(plugin->rugin->signals[i])
        };
        bl00mbox_signal_disconnect(&sig);
    }

    // pop from sets
    bl00mbox_plugin_set_always_render(plugin, false);
    bl00mbox_set_remove(&chan->plugins, plugin);

    if(plugin == chan->channel_plugin) chan->channel_plugin = NULL;

    plugin->rugin->descriptor->destroy_plugin_instance(plugin->rugin);
    free(plugin);
}


static bl00mbox_connection_t * create_connection(bl00mbox_signal_t * source){
    if(!signal_is_output(source)) return NULL;
    bl00mbox_connection_t * ret = calloc(1, sizeof(bl00mbox_connection_t));
    if(ret == NULL) return NULL;
    ret->subscribers.key = signal_equals;
    memcpy(&ret->source, source, sizeof(bl00mbox_signal_t));

    if(!bl00mbox_set_add_skip_unique_check(&source->plugin->channel->connections, ret)){
        free(ret);
        return NULL;
    }

    bl00mbox_channel_t * chan = source->plugin->channel;
    bl00mbox_take_lock(&chan->render_lock);
    source->rignal->buffer = &ret->buffer;
    bl00mbox_give_lock(&chan->render_lock);
    return ret;
}

static bl00mbox_connection_t * weak_create_connection(bl00mbox_signal_t * source){
    bl00mbox_connection_t * conn = conn_from_signal(source);
    if(conn) return conn;
    conn = create_connection(source);
    return conn;
}

static bool weak_delete_connection(bl00mbox_connection_t * conn){
    // are there still active connections?
    if(conn->subscribers.len || conn->connected_to_mixer) return false;

    bl00mbox_channel_t * chan = conn->source.plugin->channel;

    // disconnect buffer from plugin
    bl00mbox_take_lock(&chan->render_lock);
    conn->source.rignal->buffer = NULL;
    bl00mbox_give_lock(&chan->render_lock);

    // remove from connections list
    if(!bl00mbox_set_remove(&chan->connections, conn)) bl00mbox_log_error("connection list corruption");

#ifdef BL00MBOX_DEBUG
    if(conn->subscribers.len) bl00mbox_log_error("subscribers nonempty");
#endif

    free(conn);
    return true;
}

bl00mbox_error_t bl00mbox_signal_connect_mx(bl00mbox_signal_t * signal){
    if(!signal_is_output(signal)) return BL00MBOX_ERROR_INVALID_CONNECTION;
    bl00mbox_channel_t * chan = signal->plugin->channel;

    bl00mbox_connection_t * conn = weak_create_connection(signal);
    if(!conn) goto failed;

    // check if already connected
    if(conn->connected_to_mixer) return BL00MBOX_ERROR_OK;
    conn->connected_to_mixer = true;
    if(!bl00mbox_set_add(&chan->roots, conn)) goto failed;

    update_roots(chan);
    bl00mbox_connect_mx_callback(chan->parent, signal->plugin->parent);
    bl00mbox_channel_event(chan);
    return BL00MBOX_ERROR_OK;

failed:
    if(conn){
        conn->connected_to_mixer = false;
        weak_delete_connection(conn);
    }
    bl00mbox_log_error("couldn't connect to mixer");
    return BL00MBOX_ERROR_OOM;
}

static void bl00mbox_signal_disconnect_mx(bl00mbox_signal_t * signal){
    if(!signal_is_output(signal)) return;
    bl00mbox_connection_t * conn = conn_from_signal(signal);
    if(conn == NULL) return; //not connected

    conn->connected_to_mixer = false;
    bl00mbox_channel_t * chan = signal->plugin->channel;
    bl00mbox_set_remove(&chan->roots, conn);
    update_roots(chan);

    bl00mbox_disconnect_mx_callback(chan->parent, signal->plugin->parent);
    weak_delete_connection(conn);
}

static void bl00mbox_signal_disconnect_rx(bl00mbox_signal_t * signal){
    if(!signal_is_input(signal)) return;

    // try to get connection and return if not connected
    bl00mbox_connection_t * conn = conn_from_signal(signal);
    if(!conn) return;

    bl00mbox_channel_t * chan = signal->plugin->channel;
    bl00mbox_take_lock(&chan->render_lock);
    signal->rignal->buffer = NULL;
    bl00mbox_give_lock(&chan->render_lock);

    if(signal->plugin->rugin->descriptor->id == BL00MBOX_CHANNEL_PLUGIN_ID) update_roots(chan);


    bl00mbox_set_remove(&conn->subscribers, signal);
    weak_delete_connection(conn);

    bl00mbox_disconnect_rx_callback(signal->plugin->parent, signal->index);
}

void bl00mbox_signal_disconnect_tx(bl00mbox_signal_t * signal){
    if(!signal_is_output(signal)) return;
    bl00mbox_connection_t * conn = conn_from_signal(signal);
    if(!conn) return;

    // disconnect from mixer
    if(conn->connected_to_mixer) bl00mbox_signal_disconnect_mx(signal);

    // disconnect all subscribers
    bl00mbox_set_iter_t iter;
    bl00mbox_set_iter_start(&iter, &conn->subscribers);
    bl00mbox_signal_t * sub;
    while((sub = bl00mbox_set_iter_next(&iter))){
        bl00mbox_signal_disconnect_rx(sub);
    }
}

void bl00mbox_signal_disconnect(bl00mbox_signal_t * signal){
    if(!conn_from_signal(signal)) return;
    if(signal_is_input(signal)){
        bl00mbox_signal_disconnect_rx(signal);
    }
    if(signal_is_output(signal)){
        bl00mbox_signal_disconnect_tx(signal);
        bl00mbox_signal_disconnect_mx(signal);
    }
    if(conn_from_signal(signal)) bl00mbox_log_error("connection persists after disconnect");
}

bl00mbox_error_t bl00mbox_signal_connect(bl00mbox_signal_t * some, bl00mbox_signal_t * other){
    // are signals on the same channel?
    if(some->plugin->channel != other->plugin->channel) return BL00MBOX_ERROR_INVALID_CONNECTION;
    bl00mbox_channel_t * chan = some->plugin->channel;

    // which one is input, which one is output?
    bl00mbox_signal_t * signal_rx;
    bl00mbox_signal_t * signal_tx;
    if(signal_is_input(some) && signal_is_output(other)){
        signal_rx = some;
        signal_tx = other;
    } else if(signal_is_input(other) && signal_is_output(some)){
        signal_rx = other;
        signal_tx = some;
    } else {
        return BL00MBOX_ERROR_INVALID_CONNECTION;
    }

    bl00mbox_connection_t * conn;
    if(!(conn = conn_from_signal(signal_tx))){
        if(!(conn = create_connection(signal_tx))) return BL00MBOX_ERROR_OOM;
    } else {
        if(signals_are_connected(signal_rx, signal_tx)) return BL00MBOX_ERROR_OK; // already connected
    }

    bl00mbox_signal_disconnect_rx(signal_rx);

    bl00mbox_signal_t * sub;

    sub = malloc(sizeof(bl00mbox_signal_t));
    if(!sub){
        weak_delete_connection(conn);
        return BL00MBOX_ERROR_OOM;
    }

    memcpy(sub, signal_rx, sizeof(bl00mbox_signal_t));

    bl00mbox_set_add(&conn->subscribers, sub);

    // we must block here else we could access the buffer of a constant signal
    // and think it's nonconst, which is bad
    bl00mbox_take_lock(&chan->render_lock);
    signal_rx->rignal->buffer = signal_tx->rignal->buffer;
    bl00mbox_give_lock(&chan->render_lock);

    if(signal_rx->plugin->rugin->descriptor->id == BL00MBOX_CHANNEL_PLUGIN_ID) update_roots(chan);

    bl00mbox_connect_rx_callback(signal_rx->plugin->parent, signal_tx->plugin->parent, signal_rx->index);

    bl00mbox_channel_event(chan);
    return BL00MBOX_ERROR_OK;
}

void bl00mbox_plugin_set_always_render(bl00mbox_plugin_t * plugin, bool value){
    bl00mbox_channel_t * chan = plugin->channel;
    if(plugin->always_render == value) return;
    plugin->always_render = value;

    if(value){
        bl00mbox_set_add(&chan->always_render, plugin);
    } else {
        bl00mbox_set_remove(&chan->always_render, plugin);
    }
    update_roots(chan);

    bl00mbox_channel_event(chan);
}

bl00mbox_error_t bl00mbox_signal_set_value(bl00mbox_signal_t * signal, int value){
    //while(signal->plugin->is_being_rendered) {};
    if(!signal_is_input(signal)) return BL00MBOX_ERROR_INVALID_CONNECTION;
    if(conn_from_signal(signal)) bl00mbox_signal_disconnect(signal);
    signal->rignal->value = value < -32767 ? -32767 : (value > 32767 ? 32767 : value);
    return BL00MBOX_ERROR_OK;
}

int16_t bl00mbox_signal_get_value(bl00mbox_signal_t * signal){
    //while(signal->plugin->is_being_rendered) {};
    return signal->rignal->buffer ? signal->rignal->buffer[0] : signal->rignal->value;
}
